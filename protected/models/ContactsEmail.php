<?php

/**
 * This is the model class for table "contacts_email".
 *
 * The followings are the available columns in table 'contacts_email':
 * @property integer $id
 * @property integer $name
 * @property integer $email
 * @property integer $phone
 * @property integer $address
 * @property integer $city
 * @property integer $message
 * @property integer $readed
 */
class ContactsEmail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'contacts_email';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, email, phone, address, city, message', 'required'),
			array('phone', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			array('date_input, readed', 'safe'),
			// @todo Please remove those attributes that should not be searched.
			array('id, name, email, phone, address, city, message, readed', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'email' => 'Email',
			'phone' => 'Phone',
			'address' => 'Address',
			'city' => 'City',
			'message' => 'Message',
			'readed' => 'Terbaca',
			'date_input' => 'Tanggal Input',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name);
		$criteria->compare('email',$this->email);
		$criteria->compare('phone',$this->phone);
		$criteria->compare('address',$this->address);
		$criteria->compare('city',$this->city);
		$criteria->compare('message',$this->message);
		$criteria->compare('readed',$this->readed);
		$criteria->order = 'id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ContactsEmail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
