<?php

/**
 * This is the model class for table "landing_promo_product".
 *
 * The followings are the available columns in table 'landing_promo_product':
 * @property integer $id
 * @property string $nama
 * @property string $series_info
 * @property string $image
 * @property string $brosur
 * @property integer $active
 */
class LandingPromoProduct extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'landing_promo_product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama, series_info', 'required'),
			array('active', 'numerical', 'integerOnly'=>true),
			array('nama, series_info, image, brosur', 'length', 'max'=>225),
			// The following rule is used by search().
			array('image, active', 'safe'),
			// @todo Please remove those attributes that should not be searched.
			array('id, nama, series_info, image, brosur, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'series_info' => 'Series Info',
			'image' => 'Spesifikasi',
			'brosur' => 'Thumbnail',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('series_info',$this->series_info,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('brosur',$this->brosur,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LandingPromoProduct the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
