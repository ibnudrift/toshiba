<?php
$this->breadcrumbs=array(
	'Email Brosur',
);

$this->pageHeader=array(
	'icon'=>'fa fa-bank',
	'title'=>'Email Brosur',
	'subtitle'=>'Data Email Brosur',
);

$this->menu=array(
	// array('label'=>'Add Email Brosur', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<div class="row-fluid">
	<div class="span8">
<!-- <h1>Email Brosur</h1> -->
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'bank-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'rowCssClassExpression' => '
		( $data->readed == 1 ? "grey" : null )
	',
	'columns'=>array(
		// 'id',
		'name',
		'email',
		'phone',
		// 'fax',
		'body',
		'model_product',
		array(
			'name' => 'Tanggal Input',
			'type' => 'raw',
			'value' => 'date("d-m-Y", strtotime($data->date_input))',
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
</div>
	<div class="span4">
		<?php //$this->renderPartial('/setting/page_menu') ?>
	</div>
</div>

<style>
	table tr.grey td{
		background-color: grey; color: #fff;
	}
	table tr.grey td i.fa{ color: #fff; }
</style>