<?php
$this->breadcrumbs=array(
	'Landing Slides',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Landing Slides',
	'subtitle'=>'Data Landing Slides',
);

$this->menu=array(
	// array('label'=>'Add Landing Slides', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<h1>Landing Slides</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'landing-slide-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		'title',
		// 'image',
		// 'active',
		array(
			'header'=>'Active',
			'type'=>'raw',
			'value'=>'($data->active == 1)? "Active":"Not Active"',
		),
		// 'sort',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
