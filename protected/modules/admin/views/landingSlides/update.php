<?php
$this->breadcrumbs=array(
	'Landing Slide'=>array('index'),
	// $model->title=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Landing Slide',
	'subtitle'=>'Edit Landing Slide',
);

$this->menu=array(
	array('label'=>'List Landing Slide', 'icon'=>'th-list','url'=>array('index')),
	// array('label'=>'Add Landing Slide', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Landing Slide', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>