<?php
$this->breadcrumbs=array(
	'Landing Slides'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List LandingSlide','url'=>array('index')),
	array('label'=>'Add LandingSlide','url'=>array('create')),
);
?>

<h1>Manage Landing Slides</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'landing-slide-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'title',
		'image',
		'active',
		'sort',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
