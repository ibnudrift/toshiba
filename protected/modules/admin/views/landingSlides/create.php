<?php
$this->breadcrumbs=array(
	'Landing Slide'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Landing Slide',
	'subtitle'=>'Add Landing Slide',
);

$this->menu=array(
	array('label'=>'List Landing Slide', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>