<?php
$this->breadcrumbs=array(
	'Contacts Emails'=>array('index'),
	// $model->name=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'ContactsEmail',
	'subtitle'=>'Edit ContactsEmail',
);

$this->menu=array(
	array('label'=>'List ContactsEmail', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add ContactsEmail', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View ContactsEmail', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>