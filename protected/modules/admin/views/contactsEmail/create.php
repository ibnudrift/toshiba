<?php
$this->breadcrumbs=array(
	'Contacts Emails'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'ContactsEmail',
	'subtitle'=>'Add ContactsEmail',
);

$this->menu=array(
	array('label'=>'List ContactsEmail', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>