<?php
$this->breadcrumbs=array(
	'Contact Email',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Contact Email',
	'subtitle'=>'Data Contact Email',
);

$this->menu=array(
	// array('label'=>'Add Contact Email', 'icon'=>'th-list','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<h1>Contact Email</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'contacts-email-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'rowCssClassExpression' => '
		( $data->readed == 1 ? "grey" : null )
	',
	'columns'=>array(
		// 'id',
		'name',
		'email',
		'phone',
		// 'address',
		// 'city',
		'message',
		array(
			'name' => 'Tanggal Input',
			'type' => 'raw',
			'value' => 'date("d-m-Y", strtotime($data->date_input))',
		),
		/*
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>


<style>
	table tr.grey td{
		background-color: grey; color: #fff;
	}
	table tr.grey td i.fa{ color: #fff; }
</style>