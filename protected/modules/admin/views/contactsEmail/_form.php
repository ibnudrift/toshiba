<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'contacts-email-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="widget">
<h4 class="widgettitle">Data ContactsEmail</h4>
<div class="widgetcontent">


	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5', 'readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5', 'readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'phone',array('class'=>'span5', 'readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'address',array('class'=>'span5', 'readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'city',array('class'=>'span5', 'readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'message',array('class'=>'span5', 'readonly'=>'readonly')); ?>

	<?php echo $form->dropDownListRow($model, 'readed', array(
        		'1'=>'Terbaca',
        		'0'=>'Belum Terbaca',
        	), array('empty'=>'-- Pilih --')); ?>

		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Add' : 'Save',
		)); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			// 'buttonType'=>'submit',
			// 'type'=>'info',
			'url'=>CHtml::normalizeUrl(array('index')),
			'label'=>'Batal',
		)); ?>
</div>
</div>
<div class="alert">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
</div>

<?php $this->endWidget(); ?>
