<?php
$this->breadcrumbs=array(
	'Promo Products'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Promo Product',
	'subtitle'=>'Add Promo Product',
);

$this->menu=array(
	array('label'=>'List Promo Product', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>