<?php
$this->breadcrumbs=array(
	'Promo Products'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Promo Product',
	'subtitle'=>'Edit Promo Product',
);

$this->menu=array(
	array('label'=>'List Promo Product', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Promo Product', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Promo Product', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>