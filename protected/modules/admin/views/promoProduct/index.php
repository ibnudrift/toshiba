<?php
$this->breadcrumbs=array(
	'Promo Product',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Promo Product',
	'subtitle'=>'Data Promo Product',
);

$this->menu=array(
	array('label'=>'Add Promo Product', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<h1>Promo Product</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'landing-promo-product-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		'nama',
		'series_info',
		// 'spesifikasi',
		// 'brosur',
		// 'active',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
