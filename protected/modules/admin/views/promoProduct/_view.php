<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('series_info')); ?>:</b>
	<?php echo CHtml::encode($data->series_info); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('spesifikasi')); ?>:</b>
	<?php echo CHtml::encode($data->spesifikasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('brosur')); ?>:</b>
	<?php echo CHtml::encode($data->brosur); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('active')); ?>:</b>
	<?php echo CHtml::encode($data->active); ?>
	<br />


</div>