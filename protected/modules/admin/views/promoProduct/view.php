<?php
$this->breadcrumbs=array(
	'Landing Promo Products'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List LandingPromoProduct', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add LandingPromoProduct', 'icon'=>'plus-sign','url'=>array('create')),
	array('label'=>'Edit LandingPromoProduct', 'icon'=>'pencil','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete LandingPromoProduct', 'icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>View LandingPromoProduct #<?php echo $model->id; ?></h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nama',
		'series_info',
		'spesifikasi',
		'brosur',
		'active',
	),
)); ?>
