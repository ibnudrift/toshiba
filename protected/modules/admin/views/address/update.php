<?php
$this->breadcrumbs=array(
	'Dealer & ASP Locator'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-bank',
	'title'=>'Dealer & ASP Locator',
	'subtitle'=>'Edit Dealer & ASP Locator',
);

$this->menu=array(
	array('label'=>'List Dealer & ASP Locator', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Dealer & ASP Locator', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Dealer & ASP Locator', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>