<?php

class LandingController extends ControllerLanding
{
	public $layout = '//layouts_landing/column1';

	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}	

	public function actionIndex()
	{
		$this->render('index', array(	
		));
	}

	public function actionProductDetail()
	{
		$this->pageTitle = 'Download Brosur - '.$this->pageTitle;

		$this->layout = '//layouts_landing/column2';

		$ids = $_GET['id'];
		$model = LandingPromoProduct::model()->find('t.id = :ids', array(':ids'=>$ids));

		$this->render('product_detail', array(	
			'model' => $model,
		));
	}

	public function actionBrosur()
	{
		$this->pageTitle = 'Download Brosur - '.$this->pageTitle;

		$this->layout = '//layouts_landing/column2';

		$model = new ContactForm;
		$model->scenario = 'insert';

		if(isset($_POST['ContactForm']))
		{
			$secret_key = "6LcxnCkUAAAAAKZh3rxmMVPKYTntshPRNt8WvDkx";
	        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
	        $response = json_decode($response);
	        if($response->success==false)
	        {
	          $model->addError('verifyCode', 'Silahkan verifikasi captcha yang tersedia');
	        }
			$model->attributes=$_POST['ContactForm'];
			// $files = CUploadedFile::getInstance($model,'files');
			// if ($files->name != '') {
			// 	$model->files = substr(md5(time()),0,5).'-'.$files->name;
			// }
			if(!$model->hasErrors() AND $model->validate())
			{
				// if ($files->name != '') {
				// 	$files->saveAs(Yii::getPathOfAlias('webroot').'/document/'.$model->files);
				// }
				// config email
				$messaged = $this->renderPartial('//mail/brosur',array(
					'model'=>$model,
				),TRUE);

				$inputCareer = new CareerEmail;
				$inputCareer->name = $model->name;
				$inputCareer->email = $model->email;
				$inputCareer->phone = $model->phone;
				$inputCareer->body = $model->city;
				$inputCareer->model_product = $model->model_product;
				$inputCareer->date_input = date('Y-m-d H:i:s');
				$inputCareer->save(false);

				// $files_n = array(
				// 	'src' => Yii::getPathOfAlias('webroot').'/document/'.$model->files,
				// 	'filenames' => $model->files,
				// 	);
				$config = array(
					'to'=>array($model->email, $this->setting['email']),
					'subject'=>'['.Yii::app()->name.'] Enquire Brosur from '.$model->email,
					'message'=>$messaged,
					// 'files' => $files_n,
				);

				if ($this->setting['contact_cc']) {
					$config['cc'] = array($this->setting['contact_cc']);
				}
				if ($this->setting['contact_bcc']) {
					$config['bcc'] = array($this->setting['contact_bcc']);
				}
				// kirim email
				Common::mail($config);
				Yii::app()->user->setFlash('success','<i class="fa fa-check-circle-o"></i> &nbsp;&nbsp;Permintaan Anda berhasil dikirimkan.');

				$this->refresh();
			}
		}

		$this->render('brosur', array(	
			'model' => $model,
		));
	}
	
	public function actionHubungi()
	{
		$this->pageTitle = 'Hubungi Kami - '.$this->pageTitle;

		$this->layout = '//layouts_landing/column2';

		$model = new ContactForm;
		$model->scenario = 'insert';

		if(isset($_POST['ContactForm']))
		{
			$secret_key = "6LcxnCkUAAAAAKZh3rxmMVPKYTntshPRNt8WvDkx";
	        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
	        $response = json_decode($response);
	        if($response->success==false)
	        {
	          $model->addError('verifyCode', 'Silahkan verifikasi captcha yang tersedia');
	        }
			$model->attributes=$_POST['ContactForm'];
			// $files = CUploadedFile::getInstance($model,'files');
			// if ($files->name != '') {
			// 	$model->files = substr(md5(time()),0,5).'-'.$files->name;
			// }
			if(!$model->hasErrors() AND $model->validate())
			{
				// if ($files->name != '') {
				// 	$files->saveAs(Yii::getPathOfAlias('webroot').'/document/'.$model->files);
				// }
				// config email
				$messaged = $this->renderPartial('//mail/contact',array(
					'model'=>$model,
				),TRUE);

				$inputContact = new ContactsEmail;
				$inputContact->name = $model->name;
				$inputContact->email = $model->email;
				$inputContact->phone = $model->phone;
				$inputContact->address = $model->address;
				$inputContact->city = $model->city;
				$inputContact->message = $model->body;
				$inputContact->date_input = date('Y-m-d H:i:s');
				$inputContact->save(false);

				// $files_n = array(
				// 	'src' => Yii::getPathOfAlias('webroot').'/document/'.$model->files,
				// 	'filenames' => $model->files,
				// 	);
				$config = array(
					'to'=>array($model->email, $this->setting['email']),
					'subject'=>'['.Yii::app()->name.'] Enquire Contact from '.$model->email,
					'message'=>$messaged,
					// 'files' => $files_n,
				);

				// echo "<pre>";
				// print_r($config);
				// echo "</pre>";
				// exit;

				if ($this->setting['contact_cc']) {
					$config['cc'] = array($this->setting['contact_cc']);
				}
				if ($this->setting['contact_bcc']) {
					$config['bcc'] = array($this->setting['contact_bcc']);
				}
				// kirim email
				Common::mail($config);
				Yii::app()->user->setFlash('success','<i class="fa fa-check-circle-o"></i> &nbsp;&nbsp;Permintaan Anda berhasil dikirimkan.');
				
				$this->refresh();
			}
		}

		$this->render('hubungi', array(
			'model' => $model,
		));
	}


}


?>