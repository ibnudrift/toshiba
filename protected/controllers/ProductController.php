<?php

class ProductController extends Controller
{

	public $product, $category;

	public function actionIndex()
	{
		$criteria2=new CDbCriteria;
		$criteria2->with = array('description', 'category', 'categories');
		$criteria2->order = 'date DESC';
		$criteria2->addCondition('status = "1"');
		$criteria2->addCondition('description.language_id = :language_id');
		$criteria2->params[':language_id'] = $this->languageID;
		if ($_GET['q'] != '') {
			$criteria2->addCondition('t.tag LIKE :q OR description.name LIKE :q OR description.desc LIKE :q OR description.subtitle LIKE :q');
			$criteria2->params[':q'] = '%'.strval(htmlspecialchars($_GET['q'])).'%';
		}

		if ($_GET['category']) {
			$criteria = new CDbCriteria;
			$criteria->with = array('description');
			$criteria->addCondition('t.id = :id');
			$criteria->params[':id'] = intval($_GET['category']);
			$criteria->addCondition('t.type = :type');
			$criteria->params[':type'] = 'category';
			// $criteria->limit = 3;
			$criteria->order = 'sort ASC';
			$strCategory = PrdCategory::model()->find($criteria);

		}
		if ($strCategory !== null) {
			if ($strCategory->parent_id > 0) {
				$criteria = new CDbCriteria;
				$criteria->with = array('description');
				$criteria->addCondition('t.id = :id');
				$criteria->params[':id'] = $strCategory->parent_id;
				$criteria->addCondition('t.type = :type');
				$criteria->params[':type'] = 'category';
				// $criteria->limit = 3;
				$criteria->order = 'sort ASC';
				$strParentCategory = PrdCategory::model()->find($criteria);

				// Filter By Category
				$criteria2->addCondition('t.category_id = :category_id');
				$criteria2->params[':category_id'] = intval($_GET['category']);
			}else{
				// Filter By Parent
				$criteria2->addCondition('category.parent_id = :parent_id');
				$criteria2->params[':parent_id'] = intval($_GET['category']);
			}
		}

		$criteria3 = $criteria2;
		$criteria3->select = "t.id, t.brand_id";
		$criteria3->group = 't.id';
		$listProductId = PrdProduct::model()->findAll($criteria3);
		
		$idBrand = array();
		foreach ($listProductId as $key => $value) {
			array_push($idBrand, $value->brand_id);
		}
		$idBrand = array_unique($idBrand);
		$criteria4 = new CDbCriteria;
		$criteria4->with = array('description');
		$criteria4->addCondition('active = "1"');
		$criteria4->addCondition('description.language_id = :language_id');
		$criteria4->params[':language_id'] = $this->languageID;
		$criteria4->addInCondition('t.id', $idBrand);
		$dataBrand = Brand::model()->findAll($criteria4);

		$idProduct = array();
		foreach ($listProductId as $key => $value) {
			array_push($idProduct, $value->id);
		}
		$idProduct = array_unique($idProduct);
		$typeLabel = PrdCategory::getFilter($idProduct, $this->languageID);

		// ------------------ FILTER --------------------------
		$get = $_GET;
		unset($get['order']);
		unset($get['category']);
		$no = 1;
		foreach ($get as $key => $val) {
			$sql = array();
			if (is_array($get[$key])) {
				foreach ($get[$key] as $value) {
					$sql[] = 't.tag LIKE :filter'.$no;
					$criteria2->params[':filter'.$no] = '%'.urldecode($key).'='.urldecode($value).',%';
					$no++;
				}
				$criteria2->addCondition(implode(' OR ', $sql));
			}
		}


		// ------------------ ORDER ---------------------------
		switch ($_GET['order']) {
			case 'low-hight':
				$criteria2->order = 't.harga ASC';
				break;

			case 'hight-low':
				$criteria2->order = 't.harga DESC';
				break;
			
			default:
				$criteria2->order = 't.sort ASC, description.name ASC';
				break;
		}
		

		$pageSize = 12;
		$criteria2->select = "*";
		$criteria2->group = 't.id';
		$product = new CActiveDataProvider('PrdProduct', array(
			'criteria'=>$criteria2,
		    'pagination'=>array(
		        'pageSize'=>$pageSize,
		    ),
		));

		$this->layout='//layouts/column2';
		$this->pageTitle = $strCategory->description->name. (($strParentCategory != null) ? ' - '.$strParentCategory->description->name : '' ) .' - '.$this->pageTitle;
		$this->render('index', array(
			'product'=>$product,
			'strCategory'=>$strCategory,
			'strParentCategory'=>$strParentCategory,
			'dataBrand'=>$dataBrand,
			'typeLabel'=>$typeLabel,
		)); 
	}	

	public function actionLanding($category = 0)
	{
		$category = intval($category);
		$criteria = new CDbCriteria;
		$criteria->with = array('description');
		if ($category != 0) {
			$criteria->addCondition('t.id = :id');
			$criteria->params[':id'] = $category;
		}
		$criteria->addCondition('t.type = :type');
		$criteria->params[':type'] = 'category';
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		// $criteria->limit = 3;
		// $criteria->order = 'sort ASC';
		$strCategory = PrdCategory::model()->find($criteria);
		if($strCategory===null)
			throw new CHttpException(404,'The requested page does not exist.');

		$criteria = new CDbCriteria;
		$criteria->with = array('description');
		if ($category == 0) {
			$criteria->addCondition('t.parent_id = :parent_id');
			$criteria->params[':parent_id'] = $category;
		}else{
			$criteria->addCondition('t.id = :id');
			$criteria->params[':id'] = $category;
		}
		$criteria->addCondition('t.type = :type');
		$criteria->params[':type'] = 'category';
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		// $criteria->limit = 3;
		$criteria->order = 'sort ASC';
		$subCategory = PrdCategory::model()->findAll($criteria);

		$inArray = array();
		foreach ($subCategory as $key => $value) {
			array_push($inArray, $value->id);
		}

		$criteria=new CDbCriteria;
		$criteria->with = array('description', 'category', 'brand');
		$criteria->addCondition('status = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$criteria->addCondition('category.parent_id = :parent_id');
		$criteria->params[':parent_id'] = $category;
		$pageSize = 16;
		$criteria->order = 'date DESC';
		$criteria->group = 't.id';
		$product = new CActiveDataProvider('PrdProduct', array(
			'criteria'=>$criteria,
		    'pagination'=>array(
		        'pageSize'=>$pageSize,
		    ),
		));

		// print_r($product->getData());
		// exit;

		$this->layout='//layouts/column2';
		$this->pageTitle = $strCategory->description->name .' - '.$this->pageTitle;
		$this->render('landing', array(
			'subCategory'=>$subCategory,
			'strCategory'=>$strCategory,
			'product'=>$product,
		)); 
	}	
	
	public function actionList()
	{
		$criteria=new CDbCriteria;

		$criteria->with = array('description', 'category', 'categories');

		// Mengatur Order
		if ($_GET['order'] == 'new-old') {
			$criteria->order = 'date DESC';
		} elseif($_GET['order'] == 'old-new') {
			$criteria->order = 'date ASC';
		} elseif($_GET['order'] == 'hight-low') {
			$criteria->order = 'harga DESC';
		} elseif($_GET['order'] == 'low-hight') {
			$criteria->order = 'harga ASC';
		} elseif($_GET['order'] == 'a-z') {
			$criteria->order = 'description.title ASC';
		} elseif($_GET['order'] == 'z-a') {
			$criteria->order = 'description.title DESC';
		} else {
			$criteria->order = 'sort ASC, description.title ASC';
		}
		

		$criteria->addCondition('status = "1"');
		$criteria->addCondition('terlaris = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		// $criteria->addCondition('categoryView.language_id = :language_id');
		// $criteria->addCondition('categoryTitle.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
	
		if ($_GET['category'] != '') {
			// $criteria2 = new CDbCriteria;
			// $criteria2->addCondition('t.parent_id = :parent_id');
			// $criteria2->params[':parent_id'] = $_GET['category'];
			// $dataCategory = PrdCategory::model()->findAll($criteria2);

			// $dataIdCat = array();
			// foreach ($dataCategory as $key => $value) {
			// 	$dataIdCat[] = $value->id;
			// }

			$criteria->addCondition('categories.category_id = :category');
			$criteria->params['category'] = $_GET['category'];
		}else{
			$category = null;
		}
		if ($_GET['subcat'] != '') {
			$criteria->addCondition('categories.category_id = :category');
			$criteria->params[':category'] = $_GET['subcat'];
		}
		// if ($_GET['special'] != '') {
		// 	$criteria->addCondition('t.terbaru = :terbaru');
		// 	$criteria->params[':terbaru'] = 1;
		// }
		if ($_GET['q'] != '') {
            $criteria->addCondition('(description.name LIKE :q OR t.tag LIKE :q)');
            $criteria->params[':q'] = '%'.$_GET['q'].'%';
		}

        $criteria->group = 't.id';
		if ($_GET['pagesize'] != '') {
			$pageSize = $_GET['pagesize'];
		} else {
			$pageSize = 12;
		}
		$product = new CActiveDataProvider('PrdProduct', array(
			'criteria'=>$criteria,
		    'pagination'=>array(
		        'pageSize'=>$pageSize,
		    ),
		));

		$this->product = $product;
		$this->category = $category;

		$criteria = new CDbCriteria;
		$criteria->with = array('description');
		$criteria->addCondition('t.parent_id = :parent_id');
		$criteria->params[':parent_id'] = 0;
		$criteria->addCondition('t.type = :type');
		$criteria->params[':type'] = 'category';
		$criteria->limit = 10;
		$criteria->order = 'sort ASC';
		$categories = PrdCategory::model()->findAll($criteria);

		$this->layout='//layouts/column2';
		$this->render('list', array(
			'product'=>$product,
			'categories'=>$categories,
			'category'=>$category,
		)); 
	}	
	public function actionDetail($id)
	{
		$id = intval($id);
		$criteria=new CDbCriteria;
		$criteria->with = array('description', 'category');
		$criteria->addCondition('status = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$criteria->addCondition('t.id = :id');
		$criteria->params[':id'] = $id;
		$data = PrdProduct::model()->find($criteria);
		if($data===null)
			throw new CHttpException(404,'The requested page does not exist.');

		$criteria=new CDbCriteria;
		$criteria->addCondition('t.product_id = :product_id');
		$criteria->params[':product_id'] = $data->id;
		$criteria->order = 'id ASC';
		$attributes = PrdProductAttributes::model()->findAll($criteria);

		// $criteria=new CDbCriteria;
		// $criteria->with = array('description', 'category', 'categories');
		// $criteria->order = 'RAND()';
		// $criteria->addCondition('status = "1"');
		// $criteria->addCondition('description.language_id = :language_id');
		// $criteria->params[':language_id'] = $this->languageID;
		// $product = new CActiveDataProvider('PrdProduct', array(
		// 	'criteria'=>$criteria,
		//     'pagination'=>array(
		//         'pageSize'=>4,
		//     ),
		// ));

		$criteria = new CDbCriteria;
		$criteria->with = array('description');
		$criteria->addCondition('t.id = :id');
		$criteria->params[':id'] = $data->category_id;
		$criteria->addCondition('t.type = :type');
		$criteria->params[':type'] = 'category';
		// $criteria->limit = 3;
		$criteria->order = 'sort ASC';
		$category = PrdCategory::model()->find($criteria);

		$criteria=new CDbCriteria;
		$criteria->with = array('description', 'category', 'categories');
		$criteria->order = 'date DESC';
		$criteria->addCondition('status = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$criteria->addCondition('t.category_id = :category');
		$criteria->params[':category'] = $data->category_id;
		$criteria->addCondition('t.id != :id');
		$criteria->params[':id'] = $data->id;
		$criteria->limit = 4;
		$product = PrdProduct::model()->findAll($criteria);

		$this->pageTitle = $data->description->name.' | '.$this->pageTitle;
		$this->layout='//layouts/column2';
		$this->render('detail', array(	
			'data' => $data,
			'product' => $product,
			'attributes' => $attributes,
			'category' => $category,
		));
	}
	
	public function actionAddcart()
	{
		if ($_POST['id'] != '') {
			if ( ! $_POST['id'])
				throw new CHttpException(404,'The requested page does not exist.');

			if ($_POST['qty'] < 1){
				Yii::app()->user->setFlash('danger','Item can not be less than 1');
				$this->redirect(array('/product/detail', 'id'=>$_POST['id']));
			}

			if ($_POST['option'] != '') {
				$id = $_POST['id'].'-'.$_POST['option'];
			}else{
				$id = $_POST['id'];
			}
			$qty = $_POST['qty'];
			$optional = $_POST['optional'];
			$option = $_POST['option'];

			$model = new Cart;

			$data = PrdProduct::model()->findByPk($id);

			if (is_null($data))
				throw new CHttpException(404,'The requested page does not exist.');

			$model->addCart($id, $qty, $data->harga, $option, $optional);
			
			Yii::app()->user->setFlash('addcart',$qty);
			Yii::app()->user->setFlash('success','The item has been added to the shopping cart');
			Yii::app()->user->setFlash('openpop','1');
			$this->redirect(array('/product/detail', 'id'=>$_POST['id']));
		}else{
			$criteria=new CDbCriteria;
			$criteria->with = array('description');
			$criteria->addCondition('status = "1"');
			$criteria->addCondition('description.language_id = :language_id');
			$criteria->params[':language_id'] = $this->languageID;
			$criteria->addCondition('t.id = :id');
			$criteria->params[':id'] = $_GET['id'];
			$data = PrdProduct::model()->find($criteria);
			if($data===null)
				throw new CHttpException(404,'The requested page does not exist.');
			
			$model = new Cart;
			$cart = $model->viewCart($this->languageID);

			$this->render('addcart', array(	
				'data' => $data,
				'cart' => $cart[$_GET['id']],
			));
		}
	}

	public function actionAddcart2()
	{
		if ( ! $_GET['id'])
			throw new CHttpException(404,'The requested page does not exist.');

		$id = $_GET['id'];
		$qty = 1;
		$optional = $_POST['optional'];
		$option = $_POST['option'];

		$model = new Cart;

		$data = PrdProduct::model()->findByPk($id);

		if (is_null($data))
			throw new CHttpException(404,'The requested page does not exist.');

		$model->addCart($id, $qty, $data->harga, $option, $optional);
		
		Yii::app()->user->setFlash('addcart',$qty);
		$this->redirect(array('/product/addcart', 'id'=>$data->id));
	}

	public function actionEdit()
	{
		if ( ! $_POST['id'])
			throw new CHttpException(404,'The requested page does not exist.');

		$id = $_POST['id'];
		$qty = $_POST['qty'];
		$optional = $_POST['optional'];
		$option = $_POST['option'];

		$model = new Cart;

		$data = PrdProduct::model()->findByPk($id);

		if (is_null($data))
			throw new CHttpException(404,'The requested page does not exist.');

		$model->addCart($id, $qty, $data->harga, $option, $optional, 'edit');

		// $this->redirect(CHtml::normalizeUrl(array('/cart/shop')));
	}
	
	public function actionDestroy()
	{
		$model = new Cart;
		$model->destroyCart();
	}
	public function actionAddcompare($id)
	{
		$model = new Cart;
		$model->addCompare($id);
	}
	public function actionDeletecompare()
	{
		$model = new Cart;
		$model->deleteCompare($id);
		$this->redirect(CHtml::normalizeUrl(array('/product/index')));
	}
	public function actionViewcompare()
	{
		$model = new Cart;
		$data = $model->viewCompare($id);

		$this->layout='//layoutsAdmin/mainKosong';

		$categoryName = Product::model()->getCategoryName();

		$this->render('viewcompare', array(
			'data'=>$data,
			'categoryName'=>$categoryName,
		));
	}


}