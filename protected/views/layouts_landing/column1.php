<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts_landing/main'); ?>

<?php echo $this->renderPartial('//layouts_landing/_header', array()); ?>
<div class="block-wrap-fcss-top-conhome prelatife">
	<style>
   .embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
   iframe.video_iframe{
    z-index: 1;
   }
   .embed-container .closer{
    /* position: absolute; width: 100%; height: 100%;
    z-index: 2; opacity: 0; */
   }
   </style>
   <?php 
	$url = $this->setting['slidepromo_youtubeurl'];
	parse_str( parse_url( $url, PHP_URL_QUERY ), $rendervideo );
   ?>
   <div class='embed-container'>
	   <iframe class="video_iframe" src='https://www.youtube.com/embed/<?php echo $rendervideo['v'] ?>?autoplay=1&loop=1&playlist=<?php echo $rendervideo['v'] ?>&controls=0&showinfo=0&rel=0' frameborder='0' allowfullscreen></iframe>
	   <div class="closer"></div>
    </div>

	
	<?php /*<div class="video-header wrap">
        <div class="fullscreen-video-wrap">
            <video src="<?php echo Yii::app()->baseUrl; ?>/video/toshiba-slide.mp4" autoplay loop></video>
        </div>
    </div>*/ ?>

	<div id="carousel-example-fcs" class="full_slide_screen carousel fade" data-ride="carousel">
	  <div class="carousel-inner" role="listbox">
	  	<?php 
	  	$slides_landing = LandingSlide::model()->findAll("active = 1");
	  	?>
	  	<?php foreach ($slides_landing as $key => $value): ?>
	    <div class="item <?php echo ($key == 0)? 'active':'' ?>">
	      <div class="d-none d-sm-block">
	      	<a href="<?php echo $value->url ?>" target="_blank"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1916,325, '/images/landing_slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" class="img-fluid dekstop mx-auto d-block"></a>
	      </div>
	      <div class="d-block d-sm-none">
	      	<a href="<?php echo $value->url ?>" target="_blank"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(689,233, '/images/landing_slide/'.$value->image2 , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" class="img-fluid dekstop"></a>
	      </div>
	    </div>
	  	<?php endforeach ?>
	  </div>
	</div>

	<div class="clear"></div>
</div>
<?php echo $content ?>
<?php echo $this->renderPartial('//layouts_landing/_footer', array()); ?>

<?php $this->endContent(); ?>