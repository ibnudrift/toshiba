<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="language" content="<?php echo Yii::app()->language ?>" />
    
    <title><?php echo $this->pageTitle; ?></title>
    <meta name="keywords" content="<?php echo $this->metaKey; ?>">
    <meta name="description" content="<?php echo $this->metaDesc; ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
     
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/asset/css/screen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/asset/css/comon.css" />

    <link rel="Shortcut Icon" href="<?php echo Yii::app()->baseUrl; ?>/asset/images/favicon.png" />
    <link rel="icon" type="image/ico" href="<?php echo Yii::app()->baseUrl; ?>/asset/images/favicon.png" />
    <link rel="icon" type="image/x-icon" href="<?php echo Yii::app()->baseUrl; ?>/asset/images/favicon.png" />
    
    <script src="https://code.jquery.com/jquery-3.1.0.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.fancy-img').fancybox({
                    type        : 'image',
                    fitToView   : false,
                    autoSize    : false,
                    closeClick  : false,
                    openEffect  : 'none',
                    closeEffect : 'none',
                });
            $('.fancy').fancybox();
        });
    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/asset/bootstrap-4/css/bootstrap.min.css" />
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/asset/bootstrap-4/js/bootstrap.js"></script>
    
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/asset/css_landing/styles.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/asset/css_landing/animate.css">

    <!--  Media Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/asset/css_landing/media.styles.css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/asset/js/jquery.scrollTo-1.4.3.1-min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
                //Image hover
                $(".serv_block").on('mouseover',function(){
                        var info=$(this).find("img");
                        info.stop().animate({opacity:0.5},100);
                        $(".preloader").css({'background':'none'});
                    }
                );
                $(".serv_block").on('mouseout',function(){
                        var info=$(this).find("img");
                        info.stop().animate({opacity:1},100);
                        $(".preloader").css({'background':'none'});
                    }
                );
                                
            $(".toScroll").on('click', function(){
                var ids = $(this).attr("data-id");
                console.log(ids);
                if (ids=='home_c2') {
                     $("body, html").animate({ 
                        scrollTop: $('#home_c2').offset().top - 150
                    }, 1100);
                } else {
                    // $.scrollTo('#'+ids , 1100);
                    $("body, html").animate({ 
                        scrollTop: $('#'+ids).offset().top
                    }, 1100);
                }

                $('.outer-blok-black-menuresponss-hides').slideUp('slow');
            });
        });
    </script>
    <?php echo $this->setting['google_tools_webmaster']; ?>
    <?php echo $this->setting['google_tools_analytic']; ?>
    
    </head>
<body>
<?php 
/*
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=578821772130895";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Place this tag after the last +1 button tag. -->
<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/platform.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
*/
 ?>
    <?php echo $content ?>
    
    <div id="back-top" class="t-backtop">
    <div class="clear height-5"></div>
        <a href="#top"><i class="fa fa-chevron-up"></i></a>
    </div>
    <script type="text/javascript">
        $(window).load(function(){
        $('.t-backtop').hide();
        });
        $(function(){
            $('.t-backtop').click(function () {
                    $('body,html').animate({
                        scrollTop: 0
                    }, 800);
                    return false;
                });

            var $win = $(window);
                     $win.scroll(function () {
                         if ($win.scrollTop() == 0)
                         $('.t-backtop').hide();
                         else if ($win.height() + $win.scrollTop() != $(document).height() || $win.height() + $win.scrollTop() > 500) {
                         $('.t-backtop').show();
                         }
                     });
        });             
    </script>

    <script type="text/javascript">
        // $(function(){
        //         // pindah tulisan hero image
        //         var fullw = $(window).width();
        //         if (fullw <= 767) {
        //             $('.tops-cont-insidepage .insd-container.content-up').addClass('hidden-xs');
        //             var copyText = $('.tops-cont-insidepage .insd-container.content-up').html();
        //             $( ".outer-inside-middle-content.back-white .tops-cont-insidepage" ).after( "<div class='pindahan_text-heroimage'></div>" );
        //             $('.pindahan_text-heroimage').append(copyText);
        //         };
        // });       
    </script>
</body>
</html>