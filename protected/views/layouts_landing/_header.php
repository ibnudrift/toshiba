<header class="head">
  <div class="container">
    <div class="row no-gutters">
      <div class="col-md-2 col-7">
        <div class="logo-heads">
          <a href="https://www.toshiba-ac.co.id"><img src="<?php echo $this->assetBaseurl ?>logo-heads.jpg" alt="" class="img-fluid"></a>
        </div>
      </div>
      <div class="col-md-10 col-5">
        <div class="socials-heads text-right">
          <a href="https://www.facebook.com/toshibaacid/?ref=bookmarks"><i class="fa fa-facebook-square"></i></a>
          &nbsp;&nbsp;&nbsp;
          <a href="https://www.instagram.com/toshiba_ac_id/"><i class="fa fa-instagram"></i></a>
          &nbsp;&nbsp;&nbsp;
          <a href="https://www.youtube.com/channel/UC0w44FSSev14QEtx7sRk6GQ/featured"><i class="fa fa-youtube-play"></i></a>
        </div>
      </div>
    </div>
    <div class="clear clearfix"></div>
  </div>
</header>