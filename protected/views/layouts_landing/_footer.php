
<div class="blocks-tops-footernav">
	<div class="prelatife container">
		<div class="row no-gutters inners_listbox">
			<div class="col-md-4">
				<div class="ins_text">
					<a href="http://toshiba-ac.co.id/en/home/dealer/loc/dealer-location">CARI DEALER</a>
				</div>
			</div>
			<div class="col-md-4 borders">
				<div class="ins_text">
					<a target="_blank" href="<?php echo $this->setting['slidepromo_storeofficial'] ?>">BELI ONLINE</a>
				</div>
			</div>
			<div class="col-md-4">
				<div class="ins_text">
					<a href="<?php echo CHtml::normalizeUrl(array('/landing/brosur')); ?>">DOWNLOAD BROSUR</a>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!--/. Footer -->
<footer class="foot">
	<div class="prelatife container">
		<div class="ins_footer">
			<div class="row no-gutters socials_block">
				<div class="col-md-4 col-sm-4">
					<div class="tx_socials">
						<a target="_blank" href="https://www.facebook.com/toshibaacid/?ref=bookmarks"><i class="fa fa-facebook"></i> &nbsp;LIKE US ON FACEBOOK</a>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="tx_socials">
						<a target="_blank" href="https://www.instagram.com/toshiba_ac_id/"><i class="fa fa-instagram"></i> &nbsp;FOLLOW US ON INSTAGRAM</a>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="tx_socials">
						<a target="_blank" href="https://www.youtube.com/channel/UC0w44FSSev14QEtx7sRk6GQ/featured"><i class="fa fa-youtube-play"></i> &nbsp;SUBSCRIBE US ON YOUTUBE</a>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="bottoms_foot">
		<div class="t-copyrights text-center">
			Copyright &copy; 2018 - Copyright © 2018 Toshiba Air Conditioning Indonesia Website. <br>
			Website Design by <a href="https://markdesign.net" title="Website design surabaya">Mark Design Jakarta Surabaya</a>.
		</div>
	</div>
	<div class="clear"></div>
</footer>