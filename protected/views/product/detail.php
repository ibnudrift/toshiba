<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_product prelatife">

  <div class="block_infoBottom">
    <div class="container prelatife">
      <h2 class="sub_title_p">PRODUCTS</h2>
      <div class="row">
        <div class="col-md-4">
          <div class="clear height-15"></div>
          <p>The technology that you&rsquo;ve been expecting for.</p>
        </div>
        <div class="col-md-8 text-right">
          <div class="clear height-5"></div>
          <div class="outs_breadcrumb">
            <ol class="breadcrumb">
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">HOME</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/product/landing')); ?>">PRODUCTS</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'category'=>$category->parent->id)); ?>"><?php echo $category->parent->description->name ?></a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$category->id)); ?>"><?php echo $category->description->name ?></a></li>
              <li class="active"><?php echo $data->description->name ?></li>
            </ol>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">
    <div class="clear height-50"></div><div class="height-15"></div>
    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text padding-left-25 conts_pServices cont_pProduct">

          <div class="row">
            <div class="col-md-12">
              <div class="blocks_top_detailProduct">
                <div class="row">
                  <div class="col-md-6 col-sm-6">
                    <span>TOSHIBA <?php echo strtoupper($data->description->name) ?> AIR CONDITIONING</span>
                    <div class="clear"></div>
                    <h2><?php echo $data->description->name ?></h2>
                  </div>
                  <div class="col-md-6 col-sm-6 text-right">
                    <a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$category->id)); ?>" class="btn btn-link btnsr_back_product"><i class="fa fa-arrow-left"></i> &nbsp;Back to <?php echo $data->category->parent->description->name; ?> category</a>
                  </div>
                </div>
                <div class="clear"></div>
              </div>
              <div class="clear height-30"></div>

              <div class="rights_cont block_detail_products">
                  <!-- <div class="description lefts_cn"> -->
                    <?php //echo $data->description->desc ?>
                  <!-- </div> -->
                <div class="description">

                        <div class="bottoms_desc pt-0">
                          <?php if ($data->description->features != ''): ?>
                          <div class="blocks_list padding-bottom-30"><?php echo $data->description->features ?></div>
                          <?php endif ?>
                          <?php if ($data->description->desc != ''): ?>
                          <div class="blocks_list padding-bottom-30"><?php echo $data->description->desc ?></div>
                          <?php endif ?>

                          <?php /*if ($data->description->specifications != ''): ?>
                          <div class="blocks_list padding-bottom-30"><?php echo $data->description->specifications ?></div>
                          <?php endif ?>
                          <?php if ($data->description->options != ''): ?>
                          <div class="blocks_list padding-bottom-30"><?php echo $data->description->options ?></div>
                          <?php endif ?>
                          <?php if ($data->description->brochures != ''): ?>
                          <div class="blocks_list padding-bottom-30"><?php echo $data->description->brochures ?></div>
                          <?php endif*/ ?>
                        </div>

                        <div class="bottoms_desc hide hidden">
                          <ul id="myTab" class="nav nav-tabs" role="tablist">
                            <?php if ($data->description->features != ''): ?>
                            <li role="presentation"><a href="#feature" aria-controls="feature" role="tab" data-toggle="tab">FEATURES</a></li>
                            <?php endif ?>
                            
                            <?php if ($data->description->desc != ''): ?>
                            <li role="presentation"><a href="#desc" aria-controls="desc" role="tab" data-toggle="tab">DESCRIPTIONS</a></li>
                            <?php endif ?>

                            <?php if ($data->description->specifications != ''): ?>
                            <li role="presentation"><a href="#spesific" aria-controls="spesific" role="tab" data-toggle="tab">SPECIFICATIONS</a></li>
                            <?php endif ?>
                            <?php if ($data->description->options != ''): ?>
                            <li role="presentation"><a href="#option" aria-controls="option" role="tab" data-toggle="tab">REMOTE CONTROLLER</a></li>
                            <?php endif ?>
                            <?php if ($data->description->brochures != ''): ?>
                            <li role="presentation"><a href="#brochures" aria-controls="brochures" role="tab" data-toggle="tab">BROCHURES</a></li>
                            <?php endif ?>
                          </ul>
                          <div id="myTabContent" class="tab-content">
                            <?php if ($data->description->features != ''): ?>
                            <div role="tabpanel" class="tab-pane" id="feature">
                              <?php echo $data->description->features ?>
                              <div class="clear"></div>
                            </div>
                            <?php endif ?>

                            <?php if ($data->description->desc != ''): ?>
                            <div role="tabpanel" class="tab-pane" id="desc">
                              <?php echo $data->description->desc ?>
                            </div>
                            <?php endif ?>

                            <?php if ($data->description->specifications != ''): ?>
                            <div role="tabpanel" class="tab-pane" id="spesific">
                              <?php echo $data->description->specifications ?>
                            </div>
                            <?php endif ?>
                            <?php if ($data->description->options != ''): ?>
                            <div role="tabpanel" class="tab-pane" id="option">
                              <?php echo $data->description->options ?>
                            </div>
                            <?php endif ?>
                            <?php if ($data->description->brochures != ''): ?>
                            <div role="tabpanel" class="tab-pane" id="brochures">
                              <?php echo $data->description->brochures ?>
                            </div>
                            <?php endif ?>

                          </div>
                        </div>
                        <!-- End bottom descriptions -->

                        <div class="clear height-30"></div>
                        <div class="row">
                          <div class="col-md-6 col-sm-6">
                            <div class="description lefts_cn hide hidden">
                              <h4><?php echo $data->description->name ?></h4>
                              <div class="clear height-10"></div>
                              <span><?php echo $data->description->subtitle ?></span>
                              <div class="clear height-10"></div>
                              <h6><?php echo nl2br($data->description->subtitle_2) ?></h6>
                              <div class="clear height-20"></div>
                              <?php if ($data->file): ?>
                              <div class="links_dn_brochure text-center"><a href="<?php echo Yii::app()->baseUrl.'/images/file/'. $data->file; ?>" target="_blank" class="btn btn-link"><i class="fa fa-download"></i> &nbsp; Download Brochure</a></div>
                              <?php endif ?>
                              <div class="clear"></div>
                            </div>
                            <div class="blocks_leftn_cbutton padding-top-30 text-center">
                              <div class="blocks_ls">
                                <div class="list padding-bottom-5">
                                  <a href="javascript:return false;" data-toggle="modal" data-target="#myModal_spesifications"><img src="<?php echo $this->assetBaseurl ?>btns_spec_prd-child.jpg" alt="button view spesification" class="img img-fluid center-block"></a>
                                </div>
                                <div class="list padding-bottom-5">
                                  <a href="javascript:return false;" data-toggle="modal" data-target="#myModal_remote"><img src="<?php echo $this->assetBaseurl ?>btns_remote_prd-child.jpg" alt="button view spesification" class="img img-fluid center-block"></a>
                                </div>
                                <div class="list padding-bottom-5">
                                  <a href="<?php echo Yii::app()->baseUrl.'/images/file/'. $data->file; ?>" target="_blank"><img src="<?php echo $this->assetBaseurl ?>btns_brochure_prd-child.jpg" alt="button view spesification" class="img img-fluid center-block"></a>
                                </div>
                              </div>
                            </div>
                            <!-- End blocks ls -->
                          </div>
                          <div class="col-md-6 col-sm-6">
                            <div class="pictures_box">
                              <div class="pict_big"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(538,1000, '/images/product/'.$data->image , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img img-fluid center-block"></div>
                            </div>
                            <!-- end left cont box -->
                          </div>
                        </div>

                      <div class="clear height-10"></div>
                    </div>
              <!-- End bottom detail desc -->

                <div class="clear"></div>
              </div>
              <!-- End rights content -->
              

            </div>
          </div>

          <div class="clear height-25"></div>
        </div>
      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal_spesifications" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><img src="<?php echo $this->assetBaseurl ?>lgo_footer_toshiba.png" alt="" class="img img-fluid"></h4>
      </div>
      <div class="modal-body">
        <h3>Spesifications</h3>
        <?php if ($data->description->specifications != ''): ?>
        <div class="blocks_list padding-bottom-10"><?php echo $data->description->specifications ?></div>
        <?php endif ?>
        <style type="text/css">
          .modal-body h3{ margin: 0; text-align: center; line-height: 1; margin-bottom: 20px; }
          .modal-body .blocks_list img{ max-width: 100%;  }
          .modal-body .blocks_list p{ margin-bottom: 0; }
        </style>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModal_remote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><img src="<?php echo $this->assetBaseurl ?>lgo_footer_toshiba.png" alt="" class="img img-fluid"></h4>
      </div>
      <div class="modal-body">
        <h3>Remote Controller</h3>
        <?php if ($data->description->brochures != ''): ?>
        <div class="blocks_list padding-bottom-10"><?php echo $data->description->brochures ?></div>
        <?php endif ?>
        <style type="text/css">
          .modal-body h3{ margin: 0; text-align: center; line-height: 1; margin-bottom: 20px; }
          .modal-body .blocks_list img{ max-width: 100%;  }
          .modal-body .blocks_list p{ margin-bottom: 0; }
        </style>
      </div>
    </div>
  </div>
</div>

<!-- <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/asset/js/retina.min.js"></script> -->

<!-- <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/asset/js/bootstrap-tabcollapse.js"></script> -->
<script type="text/javascript">
$(document).ready(function() {
  // $('#myTab').tabCollapse();
  $('.bottoms_desc .nav.nav-tabs li:eq(0)').addClass('active');
  $('.bottoms_desc .tab-content .tab-pane:eq(0)').addClass('active');
})
</script>

<style type="text/css" media="screen">
  .links_dn_brochure{ text-align: center; display: block;  }
  .links_dn_brochure a{
    margin: 0;
    padding: 8px 20px 0;
    display: inline-block;
    text-align: center;
    background-color: #ed1b24;
    font-family: 'Roboto Condensed',sans-serif;
    font-size: 12px; text-transform: uppercase;
    font-weight: 400;
    color: #fff;
    min-width: 142px;
    height: 33px;
  }

  .links_dn_brochure a:hover,
  .links_dn_brochure a:focus{
    background-color: #ed1b24;
    text-decoration: none; color: rgba(255,255,255, 0.7);
  }
  .links_dn_brochure a i.fa{
    color: #fff; font-size: 13px;
  }
  .conts_pServices .rights_cont.block_detail_products,
  .block_detail_products .description{ padding-left: 0; }
  .conts_pServices .rights_cont.block_detail_products{ padding-left: 20px;  }
</style>