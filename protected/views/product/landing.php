<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_product prelatife" style="background-image: url('<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1883,273, '/images/static/'.$this->setting['product_banner_image'], array('method' => 'resize', 'quality' => '90')) ?>')">

  <div class="block_infoBottom">
    <div class="container prelatife">
      <h2 class="sub_title_p"><?php echo $this->setting['product_banner_title'] ?></h2>
      <div class="row">
        <div class="col-md-6">
          <div class="clear height-15"></div>
          <p><?php echo $this->setting['product_banner_subtitle'] ?></p>
        </div>
        <div class="col-md-6 text-right">
          <div class="clear height-5"></div>
          <div class="outs_breadcrumb">
            <?php if ($_GET['category']): ?>
              <ol class="breadcrumb">
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">HOME</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/product/landing')); ?>">PRODUCTS</a></li>
                <?php foreach ($subCategory as $key => $value): ?>
                <?php if ($_GET['category'] == $value->id): ?>
                  <li class="active"><?php echo strtoupper($value->description->name); ?></li>
                <?php endif ?>
                <?php endforeach; ?>
              </ol>
            <?php else: ?>
              <ol class="breadcrumb">
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">HOME</a></li>
                <li class="active">PRODUCTS</li>
              </ol>
            <?php endif ?>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">
    <div class="clear height-50"></div><div class="height-15"></div>
    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text conts_pServices cont_pProduct">
          <?php 
          $picture_cat = array(
                    'prd_residential.jpg',
                    'prd_light.jpg',
                    'prd_vrf.jpg',
                    'prd_vrf.jpg',
                    'prd_vrf.jpg',
            );
          ?>
          <div class="row">
            <div class="col-md-12">
              <div class="rights_cont">
                <!-- <h6>AIR HANDLERS</h6>
                <div class="clear height-5"></div> -->
				<?php foreach ($subCategory as $key => $value): ?>
<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.parent_id = :parent_id');
$criteria->params[':parent_id'] = $value->id;
$criteria->addCondition('t.type = :type');
$criteria->params[':type'] = 'category';
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $criteria->limit = 3;
$criteria->order = 'sort ASC';
$subCategory2 = PrdCategory::model()->findAll($criteria);

?>
                <div class="tops_landing_products landing">
                  <div class="tops_n">
                    <div class="row">
                      <div class="col-md-6 col-sm-6">
                        <div class="info_top">
                          <h6><a href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'category'=>$value->id, 'lang'=>Yii::app()->language)); ?>"><?php echo $value->description->name ?></a></h6>
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6">
                        <?php if ($value->id  == 6): ?>
                        <div class="pic_topRight"><img src="<?php echo $this->assetBaseurl ?>ex_toplanding_exproduct-1.png" alt="" class="img img-fluid"></div>
                        <?php endif ?>
                        <?php if ($value->id  == 5): ?>
                        <div class="pic_topRight"><img src="<?php echo $this->assetBaseurl ?>ex_toplanding_exproduct-2.png" alt="" class="img img-fluid"></div>
                        <?php endif ?>
                        <?php if ($value->id  == 7): ?>
                        <div class="pic_topRight"><img src="<?php echo $this->assetBaseurl ?>ex_toplanding_exproduct-3.png" alt="" class="img img-fluid"></div>
                        <?php endif ?>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="info blocks_info_CategoryProducts">
                        <div class="lists_ln row">
                      <?php foreach ($subCategory2 as $k => $v): ?>
                        <div class="col-md-3 col-sm-6 col-6">
                            <div class="itm">
                              <div class="picture">
                                <a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$v->id)); ?>">
                                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(208,208, '/images/category/'.$v->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $v->description->name ?>" class="img img-fluid">
                                </a>
                              </div>
                              <div class="names"><a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$v->id)); ?>"><span><?php echo $v->description->name ?></span></a></div>
                            </div>
                          </div>
                        <?php endforeach ?>
                        </div>
                        <div class="clear"></div>
                      </div>
                    </div>
                  </div>
                </div>

                <?php endforeach ?>

                <div class="clear height-30"></div>
              </div>
              <!-- End rights content -->

            </div>
          </div>

          <div class="clear height-50"></div><div class="height-50"></div>
        </div>
      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>





