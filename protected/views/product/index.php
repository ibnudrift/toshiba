<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_product prelatife" style="background-image: url('<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1883,273, '/images/static/'.$this->setting['product_banner_image'], array('method' => 'resize', 'quality' => '90')) ?>')">

  <div class="block_infoBottom">
    <div class="container prelatife">
      <h2 class="sub_title_p"><?php echo $this->setting['product_banner_title'] ?></h2>
      <div class="row">
        <div class="col-md-6">
          <div class="clear height-15"></div>
          <p><?php echo $this->setting['product_banner_subtitle'] ?></p>
        </div>
        <div class="col-md-6 text-right">
          <div class="clear height-5"></div>
          <div class="outs_breadcrumb">
            <ol class="breadcrumb">
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">HOME</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/product/landing')); ?>">PRODUCTS</a></li>
              <?php if ($strCategory != null): ?>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'category'=>$strCategory->parent->id)); ?>"><?php echo strtoupper($strCategory->parent->description->name) ?></a></li>
              <li class="active"><?php echo strtoupper($strCategory->description->name) ?></li>
              <?php endif ?>
            </ol>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">
    <div class="clear height-50"></div><div class="height-15"></div>
    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text conts_pServices cont_pProduct">

          <div class="row">
            <div class="col-md-12">
              <div class="rights_cont">
                <?php if ($strCategory != null): ?>
                  <?php if ($_GET['q'] != ''): ?>
                  <h6>Search for "<?php echo $_GET['q'] ?>" in <?php echo $strCategory->description->name ?><?php if ($strParentCategory != null): ?> - <?php echo $strParentCategory->description->name ?><?php endif ?></h6>
                  <?php else: ?>
                  <h6><?php if ($strParentCategory != null): ?><?php echo $strParentCategory->description->name ?> &nbsp;/&nbsp;&nbsp; <?php endif ?><?php echo $strCategory->description->name ?></h6>
                  <?php endif ?>
                <?php elseif($_GET['q'] != ''): ?>
                <h6>Searching for "<?php echo $_GET['q'] ?>" in Category <?php echo $strCategory->description->name ?><?php if ($strParentCategory != null): ?> - <?php echo $strParentCategory->description->name ?><?php endif ?></h6>
                <?php else: ?>
                <h6>Search All Products</h6>
                <?php endif ?>
                <div class="clear height-5"></div>

                    <?php if ($strCategory != null): ?>
                <div class="tops_landing_products">
                  <div class="row">
                    <?php /*<div class="col-md-6 col-sm-6">
                      <div class="info">
                        <h5><?php echo nl2br($strCategory->description->desc) ?></h5>
                        <div class="clear"></div>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                      <div class="bloc_img_right"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(555,1000, '/images/category/'.$strCategory->image , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img img-fluid"></div>
                    </div> */ ?>
                    <div class="col-md-12 col-sm-12">
                    <?php /*
                    <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1600,575, '/images/category/'.$strCategory->image2 , array('method' => 'resize', 'quality' => '90')) ?>" alt="<?php echo $strCategory->description->name ?>" class="img img-fluid center-block">*/ ?>
                    <?php
                    $criteria=new CDbCriteria;
                    $criteria->with = array('description');
                    $criteria->addCondition('description.language_id = :language_id');
                    $criteria->params[':language_id'] = $this->languageID;
                    $criteria->addCondition('topik_id = :type');
                    $criteria->params[':type'] = $strCategory->id;
                    $criteria->group = 't.id';
                    $criteria->order = 't.id ASC';
                    $slide = Slide::model()->with(array('description'))->findAll($criteria);
                    ?>
                    <?php if ($slide): ?>
                    <div id="carousel-exn_banner" class="carousel fade" data-ride="carousel" data-interval="3500">
                      <ol class="carousel-indicators">
                        <?php foreach($slide as $key => $value): ?>
                        <li data-target="#carousel-exn_banner" data-slide-to="<?php echo $key ?>" <?php echo ($key == 0)? 'class="active"':''; ?>></li>
                        <?php endforeach ?>
                      </ol>
                      <div class="carousel-inner" role="listbox">
                        <?php foreach($slide as $key => $value): ?>
                        <div class="item <?php if ($key == 0): ?>active<?php endif ?>">
                          <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1600,575, '/images/slide/'.$value->image, array('method' => 'resize', 'quality' => '90')) ?>" alt="<?php echo $strCategory->description->name ?>">
                        </div>
                        <?php endforeach ?>
                      </div>
                    </div>
                    <!-- end slide banner -->
                    <?php endif ?>
                    </div>
                  </div>
                </div>
                    <?php endif ?>

                <div class="middles_products">
                  <div class="clear height-35"></div>
                  <div class="row">
                    <div class="col-md-3 col-sm-3">
<?php
$get = array();
if ($_GET['category'] != '') {
  $get['category'] = $_GET['category'];
}
?>
                    <form action="<?php echo $this->createUrl('/product/index', $get) ?>" method="get" id="form-filter">
                    <?php // echo $this->renderPartial('//home/_left_productCat', array('category_id' => $strCategory->id)); ?>
                      <div class="lefts_c">
                        <h6>REFINE BY</h6>
                        <div class="filtering_data">
                        <?php foreach ($typeLabel as $key => $value): ?>
                          
                          <div class="list_filter">
                            <p><b><?php echo $value['data']->name ?></b></p>
                            <?php foreach ($value['child'] as $val): ?>
                            <?php if ($val): ?>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" class="select-filter" name="<?php echo urlencode($value['data']->name) ?>[]" value="<?php echo urlencode($val) ?>"  <?php if (isset($_GET[urlencode($value['data']->name)]) AND in_array(urlencode($val), $_GET[urlencode($value['data']->name)])): ?>checked<?php endif ?>>
                                <?php echo $val ?> 
                              </label>
                            </div>
                            <?php endif ?>
                            <?php endforeach ?>

                            <div class="clear"></div>
                          </div>
                        <?php endforeach ?>

                          <div class="clear"></div>
                        </div>

                        <div class="clear"></div>
                      </div>
                    </form>
                      <!-- End Left c -->
                    </div>
                    <div class="col-md-9 col-sm-9">
                      <div class="rights_c">
                        <h6><?php echo $product->getTotalItemCount() ?> PRODUCTS</h6>
                      <?php if ($product->getTotalItemCount() > 0): ?>
                        <div class="lists_sub_products">
<?php
$data = $product->getData();
?>
                          <?php foreach ($data as $key => $value): ?>
                            <div class="items">
                              <div class="row">
                                <div class="col-md-7">
                                  <div class="pict"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(400,1000, '/images/product/'.$value->image , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img img-fluid center-block"></div>
                                </div>
                                <div class="col-md-5">
                                  <div class="info text-center">
                                    <div class="titles"><?php echo $value->description->name ?></div>
                                    <span class="cat"><?php echo $value->description->subtitle ?></span>
                                    <a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>" class="btn btn-link btns_more_prd">LEARN MORE</a>
                                    <div class="clear"></div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          <?php endforeach ?>
                            <div class="clear"></div>
                        </div>
                        <div class="clear height-30"></div>
                        <!-- End list sub products -->
                        <?php $this->widget('CLinkPager', array(
                            'pages' => $product->getPagination(),
                            'header' => '',
                            'htmlOptions' => array('class'=>'pagination'),
                            'selectedPageCssClass' => 'active',
                        )) ?>

                <?php else: ?>
                <h3 class="text-center">No Data</h3>
                <?php endif ?>

                        <div class="clear"></div>
                      </div>
                      <!-- End Right c -->
                    </div>
                  </div>

                  <div class="clear"></div>
                </div>

                <div class="clear"></div>
              </div>
              <!-- End rights content -->

            </div>
          </div>

          <div class="clear height-50"></div><div class="height-50"></div>
        </div>
      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>



<script type="text/javascript">
$('.select-filter').on('change', function() {
  $('#form-filter').submit();
})
</script>