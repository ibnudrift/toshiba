<!-- inside cont -->
<div class="subpage_top_banner_illustration pg_about prelatife"  style="background-image: url('<?php echo Yii::app()->baseUrl; ?>/images/static/.$this->setting['about_banner_image']; ?>')">

  <div class="block_infoBottom">
    <div class="container prelatife">
      <h2 class="sub_title_p">BLOG</h2>
      <div class="row">
        <div class="col-md-6">
          <div class="clear height-15"></div>
          <p>Things you don't want to miss</p>
        </div>
        <div class="col-md-6 text-right">
          <div class="clear height-5"></div>
          <div class="outs_breadcrumb">
            <ol class="breadcrumb">
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><?php echo Tt::t('front', 'HOME') ?></a></li>
              <li class="active">BLOG</li>
            </ol>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</div>

<div class="clear"></div>
<?php
$folder = Blog::model()->getMenu($this->languageID, 'upcoming');
$folderArchived = Blog::model()->getMenu($this->languageID, 'archived');
?>
<div class="subpage outers_middle_content1">
  <div class="prelatife block_abouttn_1">
    <div class="clear height-50"></div><div class="height-15"></div>
    <div class="prelatife container content-text conts_pServices conts_Blog">
      <div class="clear height-10"></div>
      <div class="row">
            <div class="col-md-9">
              <div class="rights_cont">
                
                <div class="box-details_blog">
                  <div class="pictures margin-bottom-25">
                    <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(980,450, '/images/blog/'.$detail->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid center-block">
                  </div>
                  <h3 class="title"><?php echo $detail->description->title; ?></h3>
                  <p><i class="fa fa-calendar"></i> &nbsp;<?php echo date('d F Y', strtotime($detail->date_input)) ?></p>
                  <?php echo $detail->description->content; ?>
                  <div class="clear"></div>
                </div>
                <div style="height: 1px; background: #eee;"></div>
                <div class="height-20"></div>
                <?php
                $url = urlencode(Yii::app()->request->hostInfo.Yii::app()->request->requestUri);
                ?>
                <img height="45" src="<?php echo Yii::app()->baseUrl ?>/asset/images/icon-share.jpg" alt="">
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url ?>"><img height="45" src="<?php echo Yii::app()->baseUrl ?>/asset/images/icon-facebook.jpg" alt=""></a>
                <a href="https://twitter.com/home?status=<?php echo $url ?>"><img height="45" src="<?php echo Yii::app()->baseUrl ?>/asset/images/icon-twitter.jpg" alt=""></a>
                <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url ?>&title=<?php echo urlencode($detail->description->title); ?>&summary=&source="><img height="45" src="<?php echo Yii::app()->baseUrl ?>/asset/images/icon-linkedin.jpg" alt=""></a>
                <a href="mailto:sales.toshiba@carrier.co.id" alt=""><img height="45" src="<?php echo Yii::app()->baseUrl ?>/asset/images/icon-telegram.jpg" alt=""></a>
                <div class="height-20"></div>
                <div style="height: 1px; background: #eee;"></div>
                <div class="height-20"></div>
                <div class="row blocks_link-pagin">
                  <div class="col-md-6">
                    <?php if ($prev != null): ?>
                    <p style="margin: 0px;">Sebelumnya</p>
                    <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=>$prev->id )); ?>"><h4 style="font-size: 27px; font-weight: 500;" class="margin-0"><?php echo $prev->description->title ?></h4></a>
                    <?php endif ?>
                  </div>
                  <div class="col-md-6 text-right">
                    <?php if ($next != null): ?>
                    <p  style="margin: 0px;">Selanjutnya</p>
                    <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=>$next->id )); ?>"><h4 style="font-size: 27px; font-weight: 500;" class="margin-0"><?php echo $next->description->title ?></h4></a>
                    <?php endif ?>
                  </div>
                </div>


                <div class="clear"></div>
              </div>
              <!-- End rights content -->

            </div>

             <div class="col-md-3">
              <div class="lefts widget-blog">
                <h5>favorite articles</h5>
                <div class="clear height-15"></div>
                <ul class="list-unstyled lists_art_rights">
                  <?php 
                  $criteria=new CDbCriteria;
                  $criteria->with = array('description');
                  $criteria->addCondition('active = "1"');
                  $criteria->addCondition('description.language_id = :language_id');
                  $criteria->params[':language_id'] = $this->languageID;

                  $criteria->order = 'date_input ASC';
                  $criteria->limit = 5;
                  $model_sfeatures = Blog::model()->findAll($criteria);
                  ?>
                  <?php foreach ($model_sfeatures as $key => $value): ?>
                  <li <?php if (isset($_GET['id']) AND $_GET['id'] == $value->id): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=>$value->id )); ?>"><?php echo $value->description->title ?></a></li>
                  <?php endforeach ?>
                </ul>
                
                <div class="clear height-50"></div>
                <h5>archived blogs</h5>
                <div class="clear height-15"></div>
                <ul class="list-unstyled">
                  <?php foreach ($folderArchived as $key => $value): ?>
                  <li <?php if ($_GET['year'] == $value['year'] AND $_GET['month'] == $value['month']): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/blog/index', 'year'=>$value['year'], 'month'=>$value['month'])); ?>"><?php echo $value['label'] ?></a></li>
                  <?php endforeach ?>
                </ul>

              </div>
            </div>
          </div>
      <!-- End pages Events Toshiba -->

      <div class="clear"></div>
    </div>
    <div class="clear height-50"></div><div class="height-10"></div>
  </div>

  <div class="clear"></div>
</div>
<!-- end inside cont -->

<style type="text/css">
  ul.lists_art_rights{ }
  ul.lists_art_rights li{ }
  ul.lists_art_rights li a{
    white-space: nowrap; clear: left;
    overflow: hidden;
    text-overflow: ellipsis;
  }
  .blocks_link-pagin a{
    color: #000;
  }
  .blocks_link-pagin a:focus,
  .blocks_link-pagin a:hover{
    text-decoration: none; color: #03aced;
  }
</style>


<?php /*
<section class="default_sc top_inside_pg_default">
  <div class="out_table">
    <div class="in_table">
      <div class="prelatife container">
        <h1 class="sub_titlepage">Berita & Artikel</h1>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</section>
<section class="default_sc insides_middleDefaultpages back-white">
  <div class="tops_filters_whitProduct_top">
    <div class="prelatife container">
      <div class="row default">
        <div class="col-md-12">
          <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="<?php echo CHtml::normalizeUrl(array('/blog/index')); ?>">Berita & Artikel</a></li>
              <li class="active"><?php echo $detail->description->title ?></li>
            </ol>
            <div class="clear"></div>
          </div>
          <!-- end pagination products -->
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
  <!-- end filter product top -->

  <div class="prelatife container">
    <div class="clear height-35"></div>
    <div class="height-5"></div>
    <div class="content-text text-center">
        
        <div class="middles text-left details_cont_news">
            <div class="prelatife blocks_title_pagearticle">
                <h1 class="title-pages title_article blacks"><?php echo $detail->description->title ?></h1>
            </div>
            <div class="clear height-5"></div>
            <span class="dates_article"><?php echo date('d F Y', strtotime($detail->date_input)) ?></span>
            <div class="clear height-25"></div>
            <div class="details_news">
                <div class="pict_full picture">
                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(980,450, '/images/blog/'.$detail->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid center-block">
                </div>

                <div class="clear height-40 hidden-xs"></div>
                <div class="clear height-25 visible-xs"></div>

                <?php echo $detail->description->content ?>
              <?php 
              $baseUrl = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl. $this->createUrl($this->route, $_GET);
              ?>

                <div class="clear height-10"></div>
                <div class="shares-text text-left p_shares_article">
                    <span class="inline-t">SHARE</span>&nbsp; / &nbsp;<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $baseUrl ?>">FACEBOOK</a>&nbsp; /
                    &nbsp;<a target="_blank" href="https://plus.google.com/share?url=<?php echo $baseUrl ?>">GOOGLE PLUS</a>&nbsp; /
                    &nbsp;<a target="_blank" href="https://twitter.com/home?status=<?php echo $baseUrl ?>">TWITTER</a>
                </div>
                
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        <!-- end blog detail -->
        <div class="clear height-35"></div>

        <div class="blocks_cont_others_articles">
          <div class="tops">
            <h5>Artikel Lainnya</h5>
            <div class="clear"></div>
          </div>
          <div class="clear"></div>
          <div class="outers_listing_newshome defaults_t">
<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('active = "1"');
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
$criteria->order = 'date_input DESC';
$criteria->limit = 3;

$dataBlog = Blog::model()->findAll($criteria);
?>
            <div class="row default">
                <?php foreach ($dataBlog as $key => $value): ?>
                <div class="col-md-4 col-sm-4">
                    <div class="items">
                        <div class="pict"><a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=>$value->id)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(313,204, '/images/blog/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid"></a></div>
                        <div class="desc">
                            <div class="titles"><a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=>$value->id)); ?>"><?php echo $value->description->title ?></a></div>
                            <div class="clear"></div>
                            <span class="dates"><?php echo date('d F Y', strtotime($value->date_input)) ?></span>
                            <div class="clear"></div>
                            <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=>$value->id)); ?>" class="btn btn-default btns_news_default">BACA</a>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
            <div class="clear"></div>
        </div>
        <!-- end listing news -->
        </div>

        <div class="clear height-30"></div>

      <div class="clear"></div>
    </div>
    <!-- end content berita artikel -->

    <div class="clear"></div>
  </div>
</section>
*/ ?>