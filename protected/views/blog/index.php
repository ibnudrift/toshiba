<!-- inside cont -->
<div class="subpage_top_banner_illustration pg_about prelatife"  style="background-image: url('<?php echo Yii::app()->baseUrl; ?>/images/static/.$this->setting['about_banner_image']; ?>')">

  <div class="block_infoBottom">
    <div class="container prelatife">
      <h2 class="sub_title_p">BLOG</h2>
      <div class="row">
        <div class="col-md-6">
          <div class="clear height-15"></div>
          <p>Things you don't want to miss</p>
        </div>
        <div class="col-md-6 text-right">
          <div class="clear height-5"></div>
          <div class="outs_breadcrumb">
            <ol class="breadcrumb">
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><?php echo Tt::t('front', 'HOME') ?></a></li>
              <li class="active">BLOG</li>
            </ol>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</div>

<div class="clear"></div>
<?php
$folder = Blog::model()->getMenu($this->languageID, 'upcoming');
$folderArchived = Blog::model()->getMenu($this->languageID, 'archived');
?>
<div class="subpage outers_middle_content1">
  <div class="prelatife block_abouttn_1">
    <div class="clear height-50"></div><div class="height-15"></div>
    <div class="prelatife container content-text conts_pServices conts_Blog">
      <div class="clear height-10"></div>
      <div class="row">
            <div class="col-md-9">
              <div class="rights_cont">
                <?php if (isset($_GET['month'])): ?>
                <h6><?php echo $dataBlog->getTotalItemCount() ?> blogs in <?php echo(Yii::app()->locale->getMonthNames()[$_GET['month']]) ?> <?php echo $_GET['year'] ?></h6>
                <?php endif ?>
                <div class="clear height-0"></div>

                <!-- Start default list data -->
                <div class="lists_data_defaults_lh">
                  <div class="row default">
                  <?php foreach ($dataBlog->getData() as $key => $value): ?>
                    
                  <div class="col-md-4 col-sm-6 col-6">
                    <div class="items">
                      <div class="picture prelatife">
                        <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=> $value->id)); ?>">
                            <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(324,213, '/images/blog/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid">
                            </a>
                      </div>
                      <div class="info">
                        <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=> $value->id)); ?>"><h6><?php echo $value->description->title ?></h6></a>
                        <p><i class="fa fa-calendar"></i> &nbsp;<?php echo date('d F Y', strtotime($value->date_input)) ?></p>
                      </div>
                    </div>
                  </div>
                  <?php if (($key + 1) % 3 == 0): ?>
                  <div class="hidden-sm hidden-md clear"></div>
                  <?php endif ?>
                  <?php if (($key + 1) % 2 == 0): ?>
                  <div class="visible-md visible-sm clear"></div>
                  <?php endif ?>
                  <?php endforeach ?>
                  </div>
                  <div class="clear"></div>
                </div>
                <!-- End default list data -->
                  <div class="text-center bgs_paginations">
                        <?php $this->widget('CLinkPager', array(
                            'pages' => $dataBlog->getPagination(),
                            'header' => '',
                        )) ?>
                  </div>

                <div class="clear"></div>
              </div>
              <!-- End rights content -->

            </div>

             <div class="col-md-3">
              <div class="lefts widget-blog">
                <h5>favorite articles</h5>
                <div class="clear height-15"></div>
                <ul class="list-unstyled lists_art_rights">
                  <?php 
                  $criteria=new CDbCriteria;
                  $criteria->with = array('description');
                  $criteria->addCondition('active = "1"');
                  $criteria->addCondition('description.language_id = :language_id');
                  $criteria->params[':language_id'] = $this->languageID;

                  $criteria->order = 'date_input DESC';
                  $criteria->limit = 5;
                  $model_sfeatures = Blog::model()->findAll($criteria);
                  ?>
                  <?php foreach ($model_sfeatures as $key => $value): ?>
                  <li <?php if (isset($_GET['id']) AND $_GET['id'] == $value->id): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=>$value->id )); ?>"><?php echo $value->description->title ?></a></li>
                  <?php endforeach ?>
                </ul>
                
                <div class="clear height-50"></div>
                <h5>archived blogs</h5>
                <div class="clear height-15"></div>
                <ul class="list-unstyled">
                  <?php foreach ($folderArchived as $key => $value): ?>
                  <li <?php if ($_GET['year'] == $value['year'] AND $_GET['month'] == $value['month']): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/blog/index', 'year'=>$value['year'], 'month'=>$value['month'])); ?>"><?php echo $value['label'] ?></a></li>
                  <?php endforeach ?>
                </ul>

              </div>
            </div>
          </div>
      <!-- End pages Events Toshiba -->

      <div class="clear"></div>
    </div>
    <div class="clear height-50"></div><div class="height-10"></div>
  </div>

  <div class="clear"></div>
</div>
<!-- end inside cont -->

<style type="text/css">
  ul.lists_art_rights{ }
  ul.lists_art_rights li{ }
  ul.lists_art_rights li a{
    white-space: nowrap; clear: left;
    overflow: hidden;
    text-overflow: ellipsis;
  }
</style>





<?php /*
<section class="default_sc top_inside_pg_default">
  <div class="out_table">
    <div class="in_table">
      <div class="prelatife container">
        <h1 class="sub_titlepage">Berita & Artikel</h1>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</section>
<section class="default_sc insides_middleDefaultpages back-white">
  <div class="prelatife container">
    <div class="clear height-50"></div><div class="height-5"></div>
    <div class="content-text text-center">
      
      <div class="outers_listing_newshome defaults_t">
            <div class="row default">
                <?php foreach ($dataBlog->getData() as $key => $value): ?>
                    <div class="col-md-4 col-sm-4">
                      <div class="items">
                          <div class="pict"><a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=>$value->id)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(313,204, '/images/blog/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid"></a></div>
                          <div class="desc">
                              <div class="titles"><a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=>$value->id)); ?>"><?php echo $value->description->title ?></a></div>
                              <div class="clear"></div>
                              <span class="dates"><?php echo date('d F Y', strtotime($value->date_input)) ?></span>
                              <div class="clear"></div>
                              <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=>$value->id)); ?>" class="btn btn-default btns_news_default">BACA</a>
                          </div>
                      </div>
                    </div>
                <?php endforeach ?>
            </div>
            <div class="clear"></div>
        </div>
        <!-- end listing news -->
        <div class="clear height-10"></div>
      <div class="clear"></div>
    </div>
    <!-- end content berita artikel -->
    <div class="text-center bgs_paginations">
          <?php $this->widget('CLinkPager', array(
              'pages' => $dataBlog->getPagination(),
              'header' => '',
          )) ?>
    </div>
    <div class="clear height-20"></div>

    <div class="clear"></div>
  </div>
</section>*/ ?>