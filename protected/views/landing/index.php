
<section class="default_sc blocks_home_contents1" id="home-content-1">
	<div class="container">
		<div class="row outers_block_texts">
			<div class="col-md-12 col-lg-6">
				<div class="inners">
					<h3><b>Toshiba AC</b> akan selalu <br> menjadi pilihan Anda dalam <br> memilih <b>AC Kualitas Dunia.</b></h3>
					<p class="hide hidden">. . . . . </p>
					<div class="clear height-10"></div>
					<ol><li>Toshiba handal dalam menciptakan teknologi ac hemat energi dan merupakan penemu AC Inverter.</li><li>Toshiba dikenal awet karena memiliki standar produksi yang sangat&nbsp;tinggi di kelasnya.</li><li>Kini produk AC Toshiba sangat lengkap dalam memberikan solusi&nbsp;secara total untuk kebutuhan residential dan commercial.</li></ol>
				</div>
			</div>
			<div class="col-md-12 col-lg-6"></div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="d-block d-sm-block d-md-block d-lg-none features_banner">
		<img src="<?php echo $this->assetBaseurl ?>back-sections_1_res.jpg" alt="" class="img-fluid">
	</div>
</section>
<div class="spacer"></div>
<section class="default_sc blocks_home_contents2" id="home-content-2">
	<div class="container">
		<div class="row outers_block_texts">
			<div class="col-md-12 col-lg-6"></div>
			<div class="col-md-12 col-lg-6">
				<div class="inners">
					<h3>AC ON TERUSSS... <br><b>Listrik Tetap Hemat.</b></h3>
					<p class="hide hidden">. . . . . </p>
					<div class="clear height-10"></div>
					<p>AC menjadi kebutuhan utama dalam sebuah hunian, apalagi di negara&nbsp; tropis seperti Indonesia. AC terus dinyalahkan seharian sehingga&nbsp;membengkaknya biaya listrik.</p>
					<p>AC Toshiba menjadi pilihan keluarga Indonesia dalam memilih AC hemat energi dalam menurunkan konsumsi listrik. <br>Tidak tanggung-tanggung, produk Toshiba Air Conditioning telah&nbsp;mendapat Label Hemat Energi Bintang 4 dalam uji Energy Efficiency Ratio (EER).</p>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="d-block d-sm-block d-md-block d-lg-none features_banner banners2">
		<img src="<?php echo $this->assetBaseurl ?>back-sections_2_res.jpg" alt="" class="img-fluid">
	</div>
</section>
<div class="spacer"></div>
<section class="default_sc blocks_home_contents3" id="home-content-3">
	<div class="container">
		<div class="row outers_block_texts">
			<div class="col-md-12 col-lg-6">
				<div class="inners">
					<h3><b>ACnya Dijamin Awet</b> <br>Sejak Dulu.</h3>
					<p class="hide hidden">. . . . . </p>
					<div class="clear height-10"></div>
					<p>Toshiba sudah dikenal oleh kalangan masyarakat Indonesia sejak dulu hingga sekarang akan keawetannya yang tidak ada duanya.</p>

					<p>Semua AC Toshiba di produksi dalam standar kualitas International dan diuji dengan serangkaian pemeriksaan ketat pada setiap produksi untuk memastikan kualitas terbaik di kelasnya sebelum di rilis ke pasar dunia.</p>
				</div>
			</div>
			<div class="col-md-12 col-lg-6"></div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="d-block d-sm-block d-md-block d-lg-none features_banner">
		<img src="<?php echo $this->assetBaseurl ?>back-sections_3_res.jpg" alt="" class="img-fluid">
	</div>
</section>
<div class="spacer"></div>
<section class="default_sc blocks_home_contents2 back-5" id="home-content-2">
	<div class="container">
		<div class="row outers_block_texts">
			<div class="col-md-12 col-lg-6"></div>
			<div class="col-md-12 col-lg-6">
				<div class="inners">
					<h3><b>Udara Bersih dan Segar</b> <br>Setiap Hari.</h3>
					<p class="hide hidden">. . . . . </p>
					<div class="clear height-10"></div>
					<p>Kesehatan Anda menjadi prioritas kami dalam memberikan kehidupan yang lebih positif, produktif dan sehat. </p>
					<p>Karena itu, AC Toshiba dilengkapi dengan teknologi dan filter canggih dalam membersihkan udara dari debu & bau tidak sedap, serta menghambat reproduksi bakteri & virus.</p>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="d-block d-sm-block d-md-block d-lg-none features_banner">
		<img src="<?php echo $this->assetBaseurl ?>back-sections_5_res.jpg" alt="" class="img-fluid">
	</div>
</section>
<div class="spacer"></div>
<section class="default_sc blocks_homeContentProducts" id="home_productsn">
	<div class="container inners_content text-center">
		<h3>OUR PRODUCT</h3>
		<div class="clear"></div>

		<div class="lists_products_default">
			<div class="row">
				<?php 
				$s_product = LandingPromoProduct::model()->findAll();
				?>
				<?php foreach ($s_product as $key => $value){ ?>
				<div class="col-md-4 col-lg-4">
					<div class="items">
						<div class="pictures"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(585,234, '/images/landing_product/'.$value->brosur , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="" class="img-fluid d-block mx-auto"></div>
						<div class="info">
							<span class="categorys"><?php echo strtoupper($value->series_info) ?></span>
							<div class="titles-inf"><?php echo strtoupper($value->nama) ?></div>
							<a href="<?php echo CHtml::normalizeUrl(array('/landing/productDetail', 'id'=>$value->id)); ?>" class="btn btn-default btns_bluedefaults">DETAIL SPESIFIKASI</a>
							<div class="clear"></div>
						</div>
					</div>
				</div>
				<?php } ?>

			</div>
		</div>
		<!-- <div class="clear height-30"></div> -->

		<div class="clear"></div>
	</div>
</section>
<div class="spacer"></div>
<section class="default_sc blocks_home_contents5" id="home-content-5">
	<div class="d-block d-sm-block d-md-block d-lg-none features_banner">
		<img src="<?php echo $this->assetBaseurl ?>backs_bottoms_careers_res.jpg" alt="" class="img-fluid">
	</div>
	<div class="container">
		<div class="row outers_block_texts">
			<div class="col-md-12 col-lg-6">
				<div class="inners">
					<h3><b>OUR CARE CENTER</b></h3>
					<p class="hide hidden">. . . . . </p>
					<div class="clear height-10"></div>
					<p>Respon cepat dan kompeten merupakan fondasi utama dalam <br>pelayanan <b>after sales Toshiba Air Conditioning</b> kepada pelanggan.</p>
					<p>Kami memberikan dukungan untuk: <br>
					General Service, Technical Support, Spare Parts Information <br>dan Maintenance Service.</p>

					<a href="<?php echo CHtml::normalizeUrl(array('/landing/hubungi')); ?>" class="btn btn-default btns_bluedefaults">HUBUNGI KAMI</a>
				</div>
			</div>
			<div class="col-md-12 col-lg-6"></div>
		</div>
		<div class="clear"></div>
	</div>
</section>
<style type="text/css">
	.spacer{
		display: none;
	}
</style>