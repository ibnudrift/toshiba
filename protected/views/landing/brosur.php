<div class="outers_cont_inside_page back-white">

	<h1 style="visibilty: none; display:none;"><?php echo $this->pageTitle; ?></h1>
	
	<section class="blocks_inner_page" id="inside_page">
		<div class="blocks_top_red">
			<div class="container">
				<div class="row">
					<div class="col-md-10">
						<h3>DOWNLOAD BROSUR</h3>
					</div>
					<div class="col-md-2 text-right">
						<div class="backs_home"><a href="<?php echo CHtml::normalizeUrl(array('/landing/index')); ?>"><i class="fa fa-chevron-left"></i> &nbsp;Back Home</a></div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<div class="container">
			
			<div class="inners_box">
				<div class="row">
					<div class="col-md-8">
						 <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
						                  'type'=>'',
						                  'enableAjaxValidation'=>false,
						                  'clientOptions'=>array(
						                      'validateOnSubmit'=>false,
						                  ),
						                  'htmlOptions' => array(
						                      'enctype' => 'multipart/form-data',
						                  ),
						              )); ?>
						   <?php echo $form->errorSummary($model, '', '', array('class'=>'alert alert-danger')); ?>
						    <?php if(Yii::app()->user->hasFlash('success')): ?>
						        <?php $this->widget('bootstrap.widgets.TbAlert', array(
						            'alerts'=>array('success'),
						        )); ?>
						    <?php endif; ?>
							<p class="red">Pilih brosur yang anda inginkan</p>
							  <div class="form-group">
							    <label for="exampleInputEmail1">Model</label>
							    <?php 
							    $source_model = LandingPromoProduct::model()->findAll();
							    $list_model = array();
							    foreach ($source_model as $key => $value) {
							    	$list_model[$value->nama] = $value->nama;
							    }
							    ?>
							    <?php echo $form->dropDownListRow($model, 'model_product', $list_model, array('class'=>'form-control')) ?>
							  </div>
							 <p class="red">Isi data diri anda di sini</p>
							 <div class="row">
							 	<div class="col-md-6 col-sm-6">
							 		<div class="form-group">
									    <?php echo $form->labelEx($model,'name'); ?>
									    <?php echo $form->textField($model, 'name', array('class'=>'form-control')); ?>
									  </div>
							 	</div>
							 	<div class="col-md-6 col-sm-6">
							 		<div class="form-group">
									    <?php echo $form->labelEx($model,'email'); ?>
									    <?php echo $form->textField($model, 'email', array('class'=>'form-control')); ?>
									  </div>
							 	</div>
							 </div>
							 <div class="row">
							 	<div class="col-md-6 col-sm-6">
							 		<div class="form-group">
									    <?php echo $form->labelEx($model,'city'); ?>
									    <?php echo $form->textField($model, 'city', array('class'=>'form-control')); ?>
									  </div>
							 	</div>
							 	<div class="col-md-6 col-sm-6">
							 		<div class="form-group">
									    <?php echo $form->labelEx($model,'phone'); ?>
									    <?php echo $form->textField($model, 'phone', array('class'=>'form-control')); ?>
									  </div>
							 	</div>
							 </div>
							 <div class="row">
							 	<div class="col-md-6 col-sm-6">
							 		<div class="g-recaptcha" data-sitekey="6LcxnCkUAAAAAO0x5thQXTEU16euq5NnFSXRNGPb"></div>
							 	</div>
							 	<div class="col-md-6 col-sm-6">
							 		<div class="text-right">
							 		<button class="btn btn-default btns_submitsn_redform">SUBMIT</button>
							 		</div>
							 	</div>
							 </div>
						<?php $this->endWidget(); ?>
						
						<div class="clear"></div>
					</div>
					<div class="col-md-4"></div>
				</div>


				<div class="clear"></div>
			</div>

			<div class="clear"></div>
		</div>

		<div class="clear"></div>
	</section>

</div>
<script src='https://www.google.com/recaptcha/api.js'></script>