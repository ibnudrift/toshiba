<div class="outers_cont_inside_page back-white">

	<h1 style="visibilty: none; display:none;"><?php echo $this->pageTitle; ?></h1>
	
	<section class="blocks_inner_page" id="inside_page">
		<div class="blocks_top_red">
			<div class="container">
				<div class="row">
					<div class="col-md-10">
						<h3>SPECIFICATION</h3>
					</div>
					<div class="col-md-2 text-right">
						<div class="backs_home"><a href="<?php echo CHtml::normalizeUrl(array('/landing/index')); ?>"><i class="fa fa-chevron-left"></i> &nbsp;Back Home</a></div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<div class="container">
			
			<div class="details_picture">
				<img src="<?php echo Yii::app()->baseUrl; ?>/images/landing_product/<?php echo $model->image ?>" alt="" class="img-fluid d-block mx-auto">
			</div>

			<div class="clear"></div>
		</div>

		<div class="clear"></div>
	</section>

</div>