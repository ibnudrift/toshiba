<div class="outers_cont_inside_page back-white">

	<h1 style="visibilty: none; display:none;"><?php echo $this->pageTitle; ?></h1>
	
	<section class="blocks_inner_page" id="inside_page">
		<div class="blocks_top_red">
			<div class="container">
				<div class="row">
					<div class="col-md-10">
						<h3>HUBUNGI KAMI</h3>
					</div>
					<div class="col-md-2 text-right">
						<div class="backs_home"><a href="<?php echo CHtml::normalizeUrl(array('/landing/index')); ?>"><i class="fa fa-chevron-left"></i> &nbsp;Back Home</a></div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<div class="container">
			
			<div class="inners_box">
				<div class="row">
					<div class="col-md-4">
						<div class="lefts_contact">
							<p class="phones"><strong>CALL OUR CARE CENTER</strong>:<br><a href="tel:08001199888"><img src="<?php echo Yii::app()->baseUrl; ?>/asset/images_landing/cns-icon-chat.png" alt="" class="img-fluid d-inline">  0800 11 99 888</a></p>

							<p><strong>Operation Hours</strong>:<br>Monday - Friday		: 08:00 - 17:00 WIB<br>Saturday, 					: Closed<br>Sunday & <br>Public Holiday</p>




							<p><strong>Service & Parts Center</strong>:<br>Jalan Agung Timur II, Blok-1,<br>No.40-41 Sunter,<br>Jakarta 14350, Indonesia</p>
							<div class="clear clearfix"></div>
						</div>

					</div>
					<div class="col-md-8">
						<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
					                  'type'=>'',
					                  'enableAjaxValidation'=>false,
					                  'clientOptions'=>array(
					                      'validateOnSubmit'=>false,
					                  ),
					                  'htmlOptions' => array(
					                      'enctype' => 'multipart/form-data',
					                  ),
					              )); ?>
					   <?php echo $form->errorSummary($model, '', '', array('class'=>'alert alert-danger')); ?>
					    <?php if(Yii::app()->user->hasFlash('success')): ?>
					        <?php $this->widget('bootstrap.widgets.TbAlert', array(
					            'alerts'=>array('success'),
					        )); ?>
					    <?php endif; ?>
							<p>EMAIL US:</p>

							 <div class="row">
							 	<div class="col-md-12 col-sm-12">
							 		<div class="form-group">
									    <?php echo $form->labelEx($model,'name'); ?>
									    <?php echo $form->textField($model, 'name', array('class'=>'form-control')); ?>
									  </div>
							 	</div>
							 </div>
							 <div class="row">
							 	<div class="col-md-6 col-sm-6">
							 		<div class="form-group">
									    <?php echo $form->labelEx($model,'email'); ?>
									    <?php echo $form->textField($model, 'email', array('class'=>'form-control')); ?>
									  </div>
							 	</div>
							 	<div class="col-md-6 col-sm-6">
							 		<div class="form-group">
									    <?php echo $form->labelEx($model,'phone'); ?>
									    <?php echo $form->textField($model, 'phone', array('class'=>'form-control')); ?>
									  </div>
							 	</div>							 	
							 </div>

							 <div class="row">
							 	<div class="col-md-12 col-sm-12">
							 		<div class="form-group">
									    <?php echo $form->labelEx($model,'address'); ?>
									    <?php echo $form->textField($model, 'address', array('class'=>'form-control')); ?>
									  </div>
							 	</div>
							 </div>
							 <div class="row">
							 	<div class="col-md-12 col-sm-12">
							 		<div class="form-group">
									    <?php echo $form->labelEx($model,'city'); ?>
									    <?php echo $form->textField($model, 'city', array('class'=>'form-control')); ?>
									  </div>
							 	</div>
							 </div>
							 <div class="row">
							 	<div class="col-md-12 col-sm-12">
							 		<div class="form-group">
									    <?php echo $form->labelEx($model,'body'); ?>
									    <?php echo $form->textArea($model, 'body', array('class'=>'form-control')); ?>
									  </div>
							 	</div>
							 </div>

							 <div class="row">
							 	<div class="col-md-6 col-sm-6">
							 		<div class="g-recaptcha" data-sitekey="6LcxnCkUAAAAAO0x5thQXTEU16euq5NnFSXRNGPb"></div>
							 	</div>
							 	<div class="col-md-6 col-sm-6">
							 		<div class="text-right">
							 		<button class="btn btn-default btns_submitsn_redform">SUBMIT</button>
							 		</div>
							 	</div>
							 </div>
						<?php $this->endWidget(); ?>
						
						<div class="clear"></div>
					</div>
				</div>


				<div class="clear"></div>
			</div>

			<div class="clear"></div>
		</div>

		<div class="clear"></div>
	</section>

</div>
<script src='https://www.google.com/recaptcha/api.js'></script>