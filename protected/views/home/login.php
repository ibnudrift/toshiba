<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_contact prelatife">

    <div class="block_infoBottom">
      <div class="container prelatife">
        <?php 
        switch ($_GET['sub']) {
          case 'dealer':
            echo '<h2 class="sub_title_p">'.Tt::t('front', 'DEALER LOGIN').'</h2>';
            break;
          case 'installer':
            echo '<h2 class="sub_title_p">'.Tt::t('front', 'INSTALLER LOGIN').'</h2>';
            break;
          
          default:
            echo '<h2 class="sub_title_p">'.Tt::t('front', 'WARRANTY REGISTRATION').'</h2>';
            break;
        }
        ?>
        <div class="row">
          <div class="col-md-6">
            <div class="clear height-15"></div>
            <p><?php echo Tt::t('front', 'Log in with your credentials.') ?></p>
          </div>
          <div class="col-md-6 text-right">
            <div class="clear height-5"></div>

          </div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">
    <div class="clear height-50"></div><div class="height-15"></div>
    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text conts_pContactus">

          <div class="clear height-25"></div>
          <div class="boxs_grey">
            <?php if ($_GET['sub'] == 'dealer'): ?>
              <div class="tops margin-bottom-50">
                <h5><?php echo Tt::t('front', 'Toshiba Airconditioning Dealer Login Form') ?></h5>
              </div>
              <div class="clear"></div>
              <?php echo $this->renderPartial('//home/_form_loginPg', array('model'=>$model)); ?>
            <?php elseif ($_GET['sub'] == 'installer'): ?>
              <div class="tops margin-bottom-50">
                <h5><?php echo Tt::t('front', 'Toshiba Airconditioning Installer Login Form') ?></h5>
              </div>
              <div class="clear"></div>
              <?php echo $this->renderPartial('//home/_form_loginPg', array('model'=>$model)); ?>
            <?php else: ?>
              <div class="tops margin-bottom-50">
                <h5><?php echo Tt::t('front', 'Toshiba Airconditioning Warranty Registration') ?></h5>
              </div>
              <div class="clear"></div>
            <?php echo $this->renderPartial('//home/_form_loginPWarranty', array('model'=>$model)); ?>
            <?php endif ?>
            <div class="clear"></div>
          </div>

          <div class="clear"></div>
        </div>

      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>
<div class="clear height-50"></div><div class="height-10"></div>