<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_product prelatife">

  <div class="block_infoBottom">
    <div class="container prelatife">
      <h2 class="sub_title_p">PRODUCTS</h2>
      <div class="row">
        <div class="col-md-4">
          <div class="clear height-15"></div>
          <p>The technology that you&rsquo;ve been expecting for.</p>
        </div>
        <div class="col-md-8 text-right">
          <div class="clear height-5"></div>
          <div class="outs_breadcrumb">
            <ol class="breadcrumb">
              <li><a href="#">HOME</a></li>
              <li><a href="#">PRODUCTS</a></li>
              <li><a href="#">LOW COMMERCIAL</a></li>
              <li><a href="#">SINGLE SPLITS</a></li>
              <li class="active">RAS-13S3KPS-IN + RAS-13S3APS-IN1</li>
            </ol>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">
    <div class="clear height-50"></div><div class="height-15"></div>
    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text padding-left-25 conts_pServices cont_pProduct">

          <div class="row">
            <div class="col-md-12">
              <div class="blocks_top_detailProduct">
                <div class="row">
                  <div class="col-md-6 col-sm-6">
                    <span>TOSHIBA SPLITS AIR CONDITIONING</span>
                    <div class="clear"></div>
                    <h2>RAS-13S3KPS-IN + RAS-13S3APS-IN1</h2>
                  </div>
                  <div class="col-md-6 col-sm-6 text-right">
                    <a href="#" class="btn btn-link btnsr_back_product"><i class="fa fa-arrow-left"></i> &nbsp;Back to Splits category</a>
                  </div>
                </div>
                <div class="clear"></div>
              </div>
              <div class="clear height-30"></div>

              <div class="rights_cont block_detail_products">
                <div class="row">
                  <div class="col-md-6 col-sm-6">
                    <div class="pictures_box">
                      <div class="pict_big"><img src="<?php echo $this->assetBaseurl ?>product-ex1.jpg" alt="" class="img img-fluid center-block"></div>
                    </div>
                    <div class="clear height-40"></div>
                    <div class="description lefts_cn">
                      <h4>RAS-13S3KPS-IN + RAS-13S3APS-IN1</h4>
                      <div class="clear height-10"></div>
                      <span>1 TR   5 STAR</span>
                      <div class="clear height-10"></div>
                      <h6>Unrivalled Performance Resulting From A<br>Genre-Leading Technology</h6>
                      <div class="clear"></div>
                    </div>
                    <!-- end left cont box -->
                  </div>
                  <div class="col-md-6 col-sm-6">
                    <div class="description lefts_cn">
                      <ul>
                        <li>Our products comply with RoHS Regulations, ensuring no restricted substances in each of the components.</li>
                        <li>With the fast increasing waste stream, we aim to minimize the impact of electronic goods on the environment. Such inspiration helps us to limit the quantity of waste stream infiltrating into the final disposal by applying the recyclable plastics.</li>
                        <li>Non-flammable &amp; non-ozone depletion, use of R410A refrigerant which is confirmed to be non-ozone depleting, non-fammable and non-toxic</li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="clear height-50"></div>
                <div class="description">

                        <div class="bottoms_desc">
                          <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#feature" aria-controls="feature" role="tab" data-toggle="tab">FEATURES</a></li>
                            <li role="presentation"><a href="#option" aria-controls="option" role="tab" data-toggle="tab">SPECIFICATIONS</a></li>
                            <li role="presentation"><a href="#document" aria-controls="document" role="tab" data-toggle="tab">DOCUMENTS</a></li>
                            <li role="presentation"><a href="#brochure" aria-controls="brochure" role="tab" data-toggle="tab">BROCHURES</a></li>
                          </ul>

                          <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="feature">
                              <ul>
                                <li>Toshiba IAQ filter &ndash; Inhibits growth of harmful bacteria and viruses like H5N1</li>
                                <li>Hi power mode ensures delivery of extra cooling rapidly without making any undesired noise</li>
                                <li>Comfort setting &ndash; 12 flap setting, 7 speed motor input and auto cleaning function</li>
                                <li>After shut down the fan operates for 20 minutes and dries the evaporator prevents mold formation</li>
                              </ul>
                              <img src="https://placehold.it/1200x500" alt="">
                              <div class="clear"></div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="option">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis quisquam, recusandae assumenda mollitia alias. Minus perferendis possimus, mollitia vitae accusantium, aperiam veritatis dolores earum consectetur, voluptatem maxime ex suscipit minima.</p>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="document">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis quisquam, recusandae assumenda mollitia alias. Minus perferendis possimus, mollitia vitae accusantium, aperiam veritatis dolores earum consectetur, voluptatem maxime ex suscipit minima.</p>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="brochure">
                              <div class="down_brochure">
                                <div class="lists"><i class="fa-brochure"></i> 39CQ PRODUCT INFORMATION BROCHURE</div>
                                <div class="lists"><i class="fa-brochure"></i> 39CQ PRODUCT INFORMATION BROCHURE</div>
                              </div>
                            </div>
                          </div>

                        </div>
                      <div class="clear height-10"></div>
                    </div>
              <!-- End bottom detail desc -->

                <div class="clear"></div>
              </div>
              <!-- End rights content -->
              

            </div>
          </div>

          <div class="clear height-25"></div>
        </div>
      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>
