<?php 
$name_services = array(
    1 => $this->setting['service_title'],
    2 => $this->setting['part_title'],
    3 => $this->setting['warranty_title'],
    // 4 => 'Call center',
    // 5 => 'Spare parts center',
  );
if ($_GET['loc'] == 3) {
  $setName = 'warranty';
} elseif ($_GET['loc'] == 2) {
  $setName = 'part';
} else {
  $setName = 'service';
}
?>

<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_product prelatife" style="background-image: url('<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['service_banner_image']; ?>')">
  <div class="block_infoBottom">
    <div class="container prelatife">
      <h2 class="sub_title_p"><?php echo $this->setting[$setName.'_banner_title'] ?></h2>
      <div class="row">
        <div class="col-md-6">
          <div class="clear height-15"></div>
          <p><?php echo $this->setting[$setName.'_banner_subtitle'] ?></p>
        </div>
        <div class="col-md-6 text-right">
          <div class="clear height-5"></div>
          <div class="outs_breadcrumb">
            <ol class="breadcrumb">
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><?php echo Tt::t('front', 'HOME') ?></a></li>
              <li class="active"><?php echo $this->setting[$setName.'_title'] ?></li>
            </ol>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">
    <div class="clear height-50"></div><div class="height-15"></div>
    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text conts_pServices txt_service">

          <div class="row">
            <div class="col-md-3">
              <div class="lefts">
                <h5><?php echo Tt::t('front', 'Service Category') ?></h5>
                <div class="clear height-15"></div>
                <ul class="list-unstyled">
                  <li <?php echo ( (!isset($_GET['loc'])) or $_GET['loc'] == 1)? 'class="active"' : ''; ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/services/', 'loc'=>1)); ?>">Service and maintenace</a></li>
                  <li <?php echo ( $_GET['loc'] == 2)? 'class="active"' : ''; ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/services/', 'loc'=>2)); ?>">Part center</a></li>
                  <li <?php echo ( $_GET['loc'] == 3)? 'class="active"' : ''; ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/services/', 'loc'=>3)); ?>">Warranty</a></li>
                  <?php /*<li <?php echo ( $_GET['loc'] == 4)? 'class="active"' : ''; ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/services/', 'loc'=>4)); ?>">Call center</a></li>
                  <li <?php echo ( $_GET['loc'] == 5)? 'class="active"' : ''; ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/services/', 'loc'=>5)); ?>">Spare parts center</a></li>*/ ?>
                </ul>
              </div>
            </div>
            <?php 
            $name_services = array(
                1 => $this->setting['service_title'],
                2 => $this->setting['part_title'],
                3 => $this->setting['warranty_title'],
                // 4 => 'Call center',
                // 5 => 'Spare parts center',
              )
            ?>
            <div class="col-md-9">
              <div class="rights_cont">
                <?php if (isset($_GET['loc'])): ?>
                  <h4><?php echo strtoupper($name_services[$_GET['loc']]) ?></h4>
                <?php else: ?>
                  <h4><?php echo strtoupper($name_services[1]) ?></h4>
                <?php endif ?>

                <div class="clear"></div>
                <?php if ($_GET['loc'] == 1 or !isset($_GET['loc'])): ?>
                  <div class="row">
                  <div class="col-md-8 col-sm-8">
                    <div class="lefts_c">
                      <?php echo $this->setting['service_content'] ?>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4">
                    <div class="rights_c">
                      <div class="items">
                        <div class="pict"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(281,1000, '/images/static/'.$this->setting['service_left_banner_image_1'] , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img img-fluid"></div>
                        <p><?php echo $this->setting['service_left_banner_title_1'] ?></p>
                      </div>
                      <div class="clear height-20"></div>
                      <div class="items">
                        <div class="pict"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(281,1000, '/images/static/'.$this->setting['service_left_banner_image_2'] , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img img-fluid"></div>
                        <p><?php echo $this->setting['service_left_banner_title_2'] ?></p>
                      </div>
                    </div>
                  </div>
                </div>
                <?php elseif ($_GET['loc'] == 3):?>
                  <div class="row">
                  <div class="col-md-12">
                    <div class="lefts_c fulls">
                      <?php echo $this->setting['warranty_content'] ?>
                      <div class="clear"></div>
                    </div>
                  </div>
                </div>
                <?php else: ?>
                  <div class="row">
                  <div class="col-md-12">
                    <div class="lefts_c fulls">
                      <?php echo $this->setting['part_content'] ?>
                    </div>
                  </div>
                </div>
                <?php endif ?>
                
                <div class="clear"></div>
              </div>
              <!-- End rights content -->

            </div>
          </div>

          <div class="clear height-50"></div>
          <div class="clear height-20"></div>
        </div>
      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>
