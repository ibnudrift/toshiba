<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_product prelatife">

  <div class="block_infoBottom">
    <div class="container prelatife">
      <h2 class="sub_title_p">PRODUCTS</h2>
      <div class="row">
        <div class="col-md-6">
          <div class="clear height-15"></div>
          <p>The technology that you&rsquo;ve been expecting for.</p>
        </div>
        <div class="col-md-6 text-right">
          <div class="clear height-5"></div>
          <div class="outs_breadcrumb">
            <ol class="breadcrumb">
              <li><a href="#">HOME</a></li>
              <li><a href="#">PRODUCTS</a></li>
              <li><a href="#">LOW COMMERCIAL</a></li>
              <li class="active">SINGLE SPLITS</li>
            </ol>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">
    <div class="clear height-50"></div><div class="height-15"></div>
    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text conts_pServices cont_pProduct">

          <div class="row">
            <div class="col-md-12">
              <div class="rights_cont">
                <h6>LOW COMMERCIAL &nbsp;/&nbsp;&nbsp;SINGLE SPLITS</h6>
                <div class="clear height-5"></div>

                <div class="tops_landing_products">
                  <div class="row">
                    <div class="col-md-6 col-sm-6">
                      <div class="info">
                        <h5>Unrivalled Performance Resulting From A<br>Genre-Leading Technology</h5>
                        <div class="clear"></div>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                      <div class="bloc_img_right"><img src="<?php echo $this->assetBaseurl ?>p_landing_products1.jpg" alt="" class="img img-fluid"></div>
                    </div>
                  </div>
                </div>

                <div class="middles_products">
                  <div class="clear height-35"></div>
                  <div class="row">
                    <div class="col-md-3 col-sm-3">
                      <div class="lefts_c">
                        <h6>FILTER BY</h6>
                        <div class="filtering_data">
                          <div class="list_filter">
                            <p><b>TYPE</b></p>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" value="">
                                Hi Wall Fixed Speed
                              </label>
                            </div>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" value="">
                                Hi Wall Inverter
                              </label>
                            </div>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" value="">
                                Inverter Console
                              </label>
                            </div>

                            <div class="clear"></div>
                          </div>

                          <div class="list_filter">
                            <p><b>CAPACITY (TR)</b></p>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" value="">
                                1
                              </label>
                            </div>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" value="">
                                1.5
                              </label>
                            </div>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" value="">
                                2
                              </label>
                            </div>

                            <div class="clear"></div>
                          </div>

                          <div class="list_filter">
                            <p><b>Function</b></p>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" value="">
                                Cooling only
                              </label>
                            </div>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" value="">
                                Cooling and heating
                              </label>
                            </div>

                            <div class="clear"></div>
                          </div>

                          <div class="clear"></div>
                        </div>

                        <div class="clear"></div>
                      </div>
                      <!-- End Left c -->
                    </div>
                    <div class="col-md-9 col-sm-9">
                      <div class="rights_c">
                        <h6>5 PRODUCTS</h6>

                        <div class="lists_sub_products">
                        <?php for ($i=0; $i < 5; $i++) { ?>
                            <div class="items">
                              <div class="row">
                                <div class="col-md-7">
                                  <div class="pict"><img src="<?php echo $this->assetBaseurl ?>product-ex1.jpg" alt="" class="img img-fluid center-block"></div>
                                </div>
                                <div class="col-md-5">
                                  <div class="info text-center">
                                    <div class="titles">RAS-13S3KPS-IN + RAS-13S3APS-IN1</div>
                                    <span class="cat">TR 5 STAR</span>
                                    <a href="<?php echo CHtml::normalizeUrl(array('/home/productDetail')); ?>" class="btn btn-link btns_more_prd">LEARN MORE</a>
                                    <div class="clear"></div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <?php } ?>
                            <div class="clear"></div>
                        </div>
                        <!-- End list sub products -->

                        <div class="clear"></div>
                      </div>
                      <!-- End Right c -->
                    </div>
                  </div>

                  <div class="clear"></div>
                </div>

                <div class="clear"></div>
              </div>
              <!-- End rights content -->

            </div>
          </div>

          <div class="clear height-50"></div><div class="height-50"></div>
        </div>
      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>
