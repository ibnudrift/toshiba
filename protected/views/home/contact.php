<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_contact prelatife" style="background-image: url('<?php echo Yii::app()->baseUrl; ?>/images/static/.$this->setting['contact_banner_image']; ?>')">

    <div class="block_infoBottom">
      <div class="container prelatife">
        <h2 class="sub_title_p"><?php echo $this->setting['contact_banner_title'] ?></h2>
        <div class="row">
          <div class="col-md-6">
            <div class="clear height-15"></div>
            <p><?php echo $this->setting['contact_banner_subtitle'] ?></p>
          </div>
          <div class="col-md-6 text-right">
            <div class="clear height-5"></div>
            <div class="outs_breadcrumb">
              <ol class="breadcrumb">
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><?php echo Tt::t('front', 'HOME') ?></a></li>
                <li class="active"><?php echo $this->setting['contact_banner_title'] ?></li>
              </ol>
            </div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">
    <div class="clear height-50"></div><div class="height-15"></div>
    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text conts_pContactus">
          <h2><?php echo $this->setting['contact_title'] ?></h2>
          <div class="row">
            <div class="col-md-6">
              <?php echo $this->setting['contact_left'] ?>
            </div>
            <div class="col-md-6">
              <?php echo $this->setting['contact_right'] ?>
            </div>
          </div>

          <div class="clear height-45"></div>
          <div class="boxs_grey">
            <div class="tops margin-bottom-50">
              <h5><?php echo Tt::t('front', 'Toshiba Airconditioning Contact Form') ?></h5>
            </div>
            <div class="clear"></div>
            <?php echo $this->renderPartial('//home/_form_contact2', array('model'=>$model)); ?>
            <div class="clear"></div>
          </div>

          <div class="clear"></div>
        </div>

      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>
<div class="clear height-50"></div><div class="height-10"></div>