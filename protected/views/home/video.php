<div class="outer_subpage_wrapper">
<div class="subpage_top_banner_illustration pg_video prelatife">

  <div class="block_infoBottom">
    <div class="container prelatife">
      <h2 class="sub_title_p">VIDEOS</h2>
      <div class="row">
        <div class="col-md-6">
          <div class="clear height-15"></div>
          <p>Browse through our step-by-step guide and useful product information.</p>
        </div>
        <div class="col-md-6 text-right">
          <div class="clear height-5"></div>
          <div class="outs_breadcrumb">
            <ol class="breadcrumb">
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><?php echo Tt::t('front', 'HOME') ?></a></li>
              <li class="active">VIDEOS</li>
            </ol>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">
    <div class="clear height-50"></div><div class="height-15"></div>
    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text conts_pServices conts_pVideo">

          <div class="row">
            <div class="col-md-12">
              <div class="rights_cont full_c">
                <h6>Showing 12 videos</h6>
                <div class="clear height-10"></div>

                <!-- Start default list data -->
                <div class="lists_data_defaults_lh videos">
                  <div class="row default">
                  <!-- static video company -->
                  <div class="col-md-3 col-sm-4 col-6">
                    <div class="items mh-0">
                      <div class="picture prelatife">
                      <!-- <video id="my-video" class="video-js" controls preload="auto" width="282" height="170"
                        poster="MY_VIDEO_POSTER.jpg" data-setup="{}">
                          <source src="<?php echo Yii::app()->baseUrl; ?>/video/Toshiba - Better Air Solutions.mp4" type='video/mp4'>
                          <source src="<?php echo Yii::app()->baseUrl; ?>/video/Toshiba - Better Air Solutions.webm" type='video/webm'>
                          <p class="vjs-no-js">
                            To view this video please enable JavaScript, and consider upgrading to a web browser that
                            <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                          </p>
                        </video> -->
                        <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://www.youtube.com/embed/0Em7029t_xs' frameborder='0' allowfullscreen></iframe></div>
                      </div>
                      <div class="info">
                        <span class="dates">17 Juli 2017</span>
                        <h6>Better Air Solutions </h6>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-4 col-6">
                    <div class="items mh-0">
                      <div class="picture prelatife">
                      <!-- <video id="my-video" class="video-js" controls preload="auto" width="282" height="170"
                        poster="MY_VIDEO_POSTER.jpg" data-setup="{}">
                          <source src="<?php echo Yii::app()->baseUrl; ?>/video/SMMS e - Toshiba Product.mp4" type='video/mp4'>
                          <source src="<?php echo Yii::app()->baseUrl; ?>/video/SMMS e - Toshiba Product.webm" type='video/webm'>
                          <p class="vjs-no-js">
                            To view this video please enable JavaScript, and consider upgrading to a web browser that
                            <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                          </p>
                        </video> -->
                        <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://www.youtube.com/embed/joyXg4JTlg4' frameborder='0' allowfullscreen></iframe></div>
                      </div>
                      <div class="info">
                        <span class="dates">10 Juli 2017</span>
                        <h6>SMMS e - Toshiba Product</h6>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-4 col-6">
                    <div class="items mh-0">
                      <div class="picture prelatife">
                      <!-- <video id="my-video" class="video-js" controls preload="auto" width="282" height="170"
                        poster="MY_VIDEO_POSTER.jpg" data-setup="{}">
                          <source src="<?php echo Yii::app()->baseUrl; ?>/video/Toshiba Air conditioner.mp4" type='video/mp4'>
                          <source src="<?php echo Yii::app()->baseUrl; ?>/video/Toshiba Air conditioner.webm" type='video/webm'>
                          <p class="vjs-no-js">
                            To view this video please enable JavaScript, and consider upgrading to a web browser that
                            <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                          </p>
                        </video> -->
                        <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://www.youtube.com/embed/5A5FjdOnES4' frameborder='0' allowfullscreen></iframe></div>
                      </div>
                      <div class="info">
                        <span class="dates">10 Juli 2017</span>
                        <h6>Toshiba Air conditioner</h6>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-4 col-6">
                    <div class="items mh-0">
                      <div class="picture prelatife">
                      <!-- <video id="my-video" class="video-js" controls preload="auto" width="282" height="170"
                        poster="MY_VIDEO_POSTER.jpg" data-setup="{}">
                          <source src="<?php echo Yii::app()->baseUrl; ?>/video/Toshiba Air Conditioning   The technology to get you cool fast.mp4" type='video/mp4'>
                          <source src="<?php echo Yii::app()->baseUrl; ?>/video/Toshiba Air Conditioning   The technology to get you cool fast.webm" type='video/webm'>
                          <p class="vjs-no-js">
                            To view this video please enable JavaScript, and consider upgrading to a web browser that
                            <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                          </p>
                        </video> -->
                        <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://www.youtube.com/embed/-TPf-XBbypg' frameborder='0' allowfullscreen></iframe></div>
                      </div>
                      <div class="info">
                        <span class="dates">10 Juli 2017</span>
                        <h6>Toshiba Air Conditioning - The technology to get you cool fast</h6>
                      </div>
                    </div>
                  </div>
                  <!-- End static video company -->

                  <?php foreach ($dataVideo->getData() as $key => $value): ?>
                  <div class="col-md-3 col-sm-4 col-6">
                    <div class="items">
                      <div class="picture prelatife">
                        <!-- <iframe width="282" height="143" src="https://www.youtube.com/embed/<?php // echo Common::getVYoutube($value->url_youtube) ?>" frameborder="0" allowfullscreen></iframe> -->
                        <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://www.youtube.com/embed/<?php echo Common::getVYoutube($value->url_youtube) ?>' frameborder='0' allowfullscreen></iframe></div>
                      </div>
                      <div class="info">
                        <span class="dates"><?php echo date('d F Y', strtotime($value->date)) ?></span>
                        <h6><?php echo $value->description->title ?></h6>
                        <p><?php echo $value->description->content ?></p>
                      </div>
                    </div>
                  </div>
                  <?php endforeach ?>
                  </div>
                  <div class="clear"></div>
                </div>
                <!-- End default list data -->

    <div class="text-center bgs_paginations">
          <?php $this->widget('CLinkPager', array(
              'pages' => $dataVideo->getPagination(),
              'header' => '',
          )) ?>
    </div>

                <div class="clear"></div>
              </div>
              <!-- End rights content -->

            </div>
          </div>

          <div class="clear height-25"></div>
        </div>
      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>

<link href="http://vjs.zencdn.net/6.2.0/video-js.css" rel="stylesheet">
<!-- If you'd like to support IE8 -->
<script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
<script src="http://vjs.zencdn.net/6.2.0/video.js"></script>