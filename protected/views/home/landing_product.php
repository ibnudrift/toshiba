<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_product prelatife">

  <div class="block_infoBottom">
    <div class="container prelatife">
      <h2 class="sub_title_p">PRODUCTS</h2>
      <div class="row">
        <div class="col-md-6">
          <div class="clear height-15"></div>
          <p>The technology that you&rsquo;ve been expecting for.</p>
        </div>
        <div class="col-md-6 text-right">
          <div class="clear height-5"></div>
          <div class="outs_breadcrumb">
            <ol class="breadcrumb">
              <li><a href="#">HOME</a></li>
              <li class="active">PRODUCTS</li>
            </ol>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">
    <div class="clear height-50"></div><div class="height-15"></div>
    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text conts_pServices cont_pProduct">

          <div class="row">
            <div class="col-md-12">
              <div class="rights_cont">
                <!-- <h6>AIR HANDLERS</h6>
                <div class="clear height-5"></div> -->

                <div class="tops_landing_products">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="info blocks_info_CategoryProducts">
                        <h5>Low Commercial</h5>
                        <div class="clear height-10"></div>
                        <h6>Category:</h6>
                        <div class="clear height-5"></div>
                        <div class="lists_ln">
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Light commercial</a>
                        &nbsp;|&nbsp;
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Residential</a>
                        &nbsp;|&nbsp;
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">VRF</a>
                        </div>
                        <div class="clear"></div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="bloc_img_right"><img src="<?php echo $this->assetBaseurl ?>p_landing_products1.jpg" alt="" class="img img-fluid"></div>
                    </div>
                  </div>
                </div>

                <div class="clear height-30"></div>
              </div>
              <!-- End rights content -->

            </div>
          </div>

          <div class="clear height-50"></div><div class="height-50"></div>
        </div>
      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>
