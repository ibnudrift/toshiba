<!-- inside cont -->
<div class="subpage_top_banner_illustration pg_about prelatife"  style="background-image: url('<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['about_banner_image']; ?>')">

  <div class="block_infoBottom">
    <div class="container prelatife">
      <h2 class="sub_title_p"><?php echo $this->setting['about_banner_title'] ?></h2>
      <div class="row">
        <div class="col-md-6">
          <div class="clear height-15"></div>
          <p><?php echo $this->setting['about_banner_subtitle'] ?></p>
        </div>
        <div class="col-md-6 text-right">
          <div class="clear height-5"></div>
          <div class="outs_breadcrumb">
            <ol class="breadcrumb">
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><?php echo Tt::t('front', 'HOME') ?></a></li>
              <li class="active"><?php echo $this->setting['about_banner_title'] ?></li>
            </ol>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</div>

<div class="clear"></div>
<div class="subpage outers_middle_content1">
  <div class="prelatife block_abouttn_1">
    <div class="clear height-50"></div><div class="height-15"></div>
    <div class="prelatife container content-text">
      <h2><?php echo $this->setting['about_title'] ?></h2>
      <div class="clear height-20"></div>
      <div class="row block_about_tx1 prelatife">
        <div class="col-md-6">
          <div class="lefts_c">
            <h6><?php echo nl2br($this->setting['about_subtitle']) ?></h6>

            <?php echo $this->setting['about_content'] ?>
            <div class="clear"></div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="rights_c">
            <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(503,1000, '/images/static/'.$this->setting['about_image'] , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img img-fluid">
            <div class="clear"></div>
          </div>
        </div>
      </div>

      <div class="clear"></div>
    </div>
    <div class="clear height-50"></div><div class="height-10"></div>
  </div>

  <div class="block_middle_contabout_2 back_cloud">
    <div class="prelatife container">
      <div class="insides">
        <div class="clear h320"></div>
        <div class="maw650 tengah">
          <div class="txt1">
            <h5><?php echo $this->setting['btm_about_title_s1'] ?></h5>
            <h2><?php echo $this->setting['btm_about_subtitle_s1'] ?></h2>
            <?php echo $this->setting['btm_about_text_s1'] ?>
            <div class="clear"></div>
          </div>
          <div class="clear height-25"></div>
          <div class="lines-horizontals_n tengah"></div>
          <div class="clear height-20"></div>
          <div class="txt2">
            <?php echo $this->setting['btm_about_text2_subtitle_s1'] ?>
            <div class="clear"></div>
          </div>
        </div>
        <div class="clear height-20"></div>
        <div class="lines-horizontals_n tengah"></div>
        <div class="clear height-30"></div>

        <div class="lists_banners_dtAbout">
          <div class="items">
            <div class="row row-eq-height">
              <div class="col-md-6">
                <div class="block_desc">
                  <div class="info">
                    <h3><?php echo $this->setting['btm_about_banner_title_1'] ?></h3>
                    <p><?php echo $this->setting['btm_about_banner_text_1'] ?></p>
                    <div class="clear"></div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="pict">
                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(580,344, '/images/static/'. $this->setting['btm_about_banner_pict_1'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid">
                </div>
              </div>
            </div>
            <div class="clear"></div>
          </div>
          <div class="items">
            <div class="row row-eq-height">
            <div class="col-md-6">
              <div class="pict">
                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(580,344, '/images/static/'. $this->setting['btm_about_banner_pict_2'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid">
              </div>
              </div>
              <div class="col-md-6">
                <div class="block_desc back_red">
                  <div class="info">
                    <h3><?php echo $this->setting['btm_about_banner_title_2'] ?></h3>
                    <p><?php echo $this->setting['btm_about_banner_text_2'] ?></p>
                    <div class="clear"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="clear"></div>
          </div>
          <div class="items">
            <div class="row row-eq-height">
              <div class="col-md-6">
                <div class="block_desc back_blue">
                  <div class="info">
                    <h3><?php echo $this->setting['btm_about_banner_title_3'] ?></h3>
                    <p><?php echo $this->setting['btm_about_banner_text_3'] ?></p>
                    <div class="clear"></div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="pict">
                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(580,344, '/images/static/'. $this->setting['btm_about_banner_pict_3'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid">
                </div>
              </div>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <div class="clear height-50"></div><div class="height-25"></div>

        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
  </div>

  <div class="clear"></div>
</div>
<!-- end inside cont -->