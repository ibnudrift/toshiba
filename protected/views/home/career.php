<section class="outer-inside-middle-content back-white">
  <div class="prelatife">
    <div class="tops-cont-insidepage prelatife"> 
        <div class="picts-fullw"><img src="<?php echo $this->assetBaseurl; ?>inside-pg-abouts-top.jpg" alt="" class="img img-fluid"></div>

        <div class="pos-abs-fullw tp50p">
          <div class="tengah insd-container text-center content-up">
             <h1 class="titles-pages-sub">Kontak Xado</h1>
             <div class="clear height-15"></div>
             <p>Sebuah inovasi untuk perlindungan dan perbaikan</p>
            <div class="clear"></div>
          </div>
        </div>
    </div>

    <div class="clear"></div>
  </div>

  <div class="back-white mh500">
    <div class="prelatife container">
        <div class="clear height-50"></div>
          <div class="height-45"></div>
          <div class="middles">
            
            <div class="mw920 tengah text-center content-text">
                <h4>Contact Us</h4> <div class="clear height-0"></div>
                <div class="clear height-25"></div>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br> Enim dolorum sunt natus consequuntur reprehenderit, <br> molestias praesentium ab ipsum eveniet fugiat voluptate at accusantium, <br> ducimus provident incidunt? Aperiam quasi omnis vel?</p>
                <div class="clear height-50"></div><div class="height-5"></div>
                
                <div class="contacts-career contact-form">
                    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                                'type'=>'horizontal',
                                'enableAjaxValidation'=>false,
                                'clientOptions'=>array(
                                    'validateOnSubmit'=>false,
                                ),
                                'htmlOptions' => array(
                                    'enctype' => 'multipart/form-data',
                                    'onsubmit'=>'javascript: return false;',
                                ),
                            )); ?>
                                <div class="height-10"></div>
                                <?php echo $form->errorSummary($model); ?>
                                <?php if(Yii::app()->user->hasFlash('success')): ?>
                                    <?php $this->widget('bootstrap.widgets.TbAlert', array(
                                        'alerts'=>array('success'),
                                    )); ?>
                                <?php endif; ?>
                                
                                <!-- name -->
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Name</label>
                                    <div class="col-sm-9 padding-0">
                                        <?php echo $form->textField($model, 'name', array('class'=>'form-control')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Phone</label>
                                    <div class="col-sm-9 padding-0">
                                        <?php echo $form->textField($model, 'phone', array('class'=>'form-control')); ?>
                                    </div>
                                </div>
                                <!-- email_address -->
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Email</label>
                                    <div class="col-sm-9 padding-0">
                                        <?php echo $form->textField($model, 'email', array('class'=>'form-control')); ?>
                                    </div>
                                </div>
                                <!-- alamat -->
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Address</label>
                                    <div class="col-sm-9 padding-0">
                                        <?php echo $form->textField($model, 'address', array('class'=>'form-control')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">City</label>
                                    <div class="col-sm-9 padding-0">
                                        <?php echo $form->textField($model, 'city', array('class'=>'form-control')); ?>
                                    </div>
                                </div>
                                <!-- country -->
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Country</label>
                                    <div class="col-sm-9 padding-0">
                                        <?php echo $form->textField($model, 'country', array('class'=>'form-control')); ?>
                                    </div>
                                </div>
                                <!-- country -->
                                <?php /*<div class="form-group">
                                    <label class="col-sm-3 control-label">CV.</label>
                                    <div class="col-sm-9 padding-0">
                                        <?php echo $form->fileField($model, 'curiculum_vitae', array('class'=>'form-control')); ?>
                                    </div>
                                </div>*/  ?>
                                <!-- body -->
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Message</label>
                                    <div class="col-sm-9 padding-0">
                                        <?php echo $form->textArea($model, 'body', array('class'=>'form-control')); ?>
                                    </div>
                                </div>

                                <!-- security code -->
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">&nbsp;</label>
                                    <div class="col-sm-9">
                                        <div class="g-recaptcha" data-sitekey="6LfGLRQTAAAAAK4IeoDwXAKmBGww7r-7exHKjDEf"></div>
                                    </div>
                                  </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">&nbsp;</label>
                                    <div class="col-sm-9">
                                        <?php $this->widget('bootstrap.widgets.TbButton', array(
                                            'buttonType'=>'submit',
                                            'htmlOptions'=>array('class'=>"btn bt-submit fleft"),
                                            'label'=>'',
                                        )); ?>
                                    </div>
                                </div>
                            <?php $this->endWidget(); ?>
                        <div class="clear"></div>
                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>

            <div class="clear"></div>
          </div>

          <div class="clear"></div>
        </div>
      
      <div class="clear"></div> <div class="height-50"></div><div class="height-20"></div>
    </div>
  </div>
</section>
<script src='https://www.google.com/recaptcha/api.js'></script>