<!-- inside cont -->
<div class="subpage_top_banner_illustration pg_about prelatife"  style="background-image: url('<?php echo Yii::app()->baseUrl; ?>/images/static/.$this->setting['about_banner_image']; ?>')">

  <div class="block_infoBottom">
    <div class="container prelatife">
      <h2 class="sub_title_p">EVENTS</h2>
      <div class="row">
        <div class="col-md-6">
          <div class="clear height-15"></div>
          <p>Things you don't want to miss</p>
        </div>
        <div class="col-md-6 text-right">
          <div class="clear height-5"></div>
          <div class="outs_breadcrumb">
            <ol class="breadcrumb">
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><?php echo Tt::t('front', 'HOME') ?></a></li>
              <li class="active">EVENTS</li>
            </ol>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</div>

<div class="clear"></div>
<?php
$folder = Event::model()->getMenu($this->languageID, 'upcoming');
$folderArchived = Event::model()->getMenu($this->languageID, 'archived');
?>
<div class="subpage outers_middle_content1">
  <div class="prelatife block_abouttn_1">
    <div class="clear height-50"></div><div class="height-15"></div>
    <div class="prelatife container content-text conts_pServices conts_pEvent">
      <div class="clear height-10"></div>
      <div class="row">
            <div class="col-md-3">
              <div class="lefts">
                <h5>upcoming events</h5>
                <div class="clear height-15"></div>
                <ul class="list-unstyled">
                  <?php foreach ($folder as $key => $value): ?>
                  <li <?php if ($_GET['year'] == $value['year'] AND $_GET['month'] == $value['month']): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/event', 'year'=>$value['year'], 'month'=>$value['month'])); ?>"><?php echo $value['label'] ?></a></li>
                  <?php endforeach ?>
                </ul>
                
                <div class="clear height-50"></div>
                <h5>archived events</h5>
                <div class="clear height-15"></div>
                <ul class="list-unstyled">
                  <?php foreach ($folderArchived as $key => $value): ?>
                  <li <?php if ($_GET['year'] == $value['year'] AND $_GET['month'] == $value['month']): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/event', 'year'=>$value['year'], 'month'=>$value['month'])); ?>"><?php echo $value['label'] ?></a></li>
                  <?php endforeach ?>
                </ul>

              </div>
            </div>
            <div class="col-md-9">
              <div class="rights_cont">
                <?php if (isset($_GET['month'])): ?>
                <h6><?php echo $dataEvent->getTotalItemCount() ?> events in <?php echo(Yii::app()->locale->getMonthNames()[$_GET['month']]) ?> <?php echo $_GET['year'] ?></h6>
                <?php endif ?>
                <div class="clear height-0"></div>

                <!-- Start default list data -->
                <div class="lists_data_defaults_lh">
                  <div class="row default">
                  <?php foreach ($dataEvent->getData() as $key => $value): ?>
                    
                  <div class="col-md-4 col-sm-6 col-6">
                    <div class="items">
                      <div class="picture prelatife">
                        <ul id="light-gallery" class="gallery list-un light-gallery">
                        <?php foreach ($value->images as $k => $v): ?>
                          <?php if ($k == 0): ?>
                          <li data-src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(800,500, '/images/event/'.$v->image , array('method' => 'resize', 'quality' => '90')) ?>">
                          <a href="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(800,500, '/images/event/'.$v->image , array('method' => 'resize', 'quality' => '90')) ?>">
                            <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(324,213, '/images/event/'.$v->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid">
                            </a>
                          </li>
                          <?php else: ?>
                          <li data-src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(800,500, '/images/event/'.$v->image , array('method' => 'resize', 'quality' => '90')) ?>"></li>
                          <?php endif ?>
                        <?php endforeach ?>
                        </ul>
                      </div>
                      <div class="info">
                        <h6><?php echo $value->description->title ?></h6>
                        <p><?php echo date('d F Y', strtotime($value->date)) ?><br>
                        <?php echo $value->description->content ?></p>
                      </div>
                    </div>
                  </div>
                  <?php if (($key + 1) % 3 == 0): ?>
                  <div class="hidden-sm hidden-md clear"></div>
                  <?php endif ?>
                  <?php if (($key + 1) % 2 == 0): ?>
                  <div class="visible-md visible-sm clear"></div>
                  <?php endif ?>
                  <?php endforeach ?>
                  </div>
                  <div class="clear"></div>
                </div>
                <!-- End default list data -->
    <div class="text-center bgs_paginations">
          <?php $this->widget('CLinkPager', array(
              'pages' => $dataEvent->getPagination(),
              'header' => '',
          )) ?>
    </div>

                <div class="clear"></div>
              </div>
              <!-- End rights content -->

            </div>
          </div>
      <!-- End pages Events Toshiba -->

      <div class="clear"></div>
    </div>
    <div class="clear height-50"></div><div class="height-10"></div>
  </div>

  <div class="clear"></div>
</div>
<!-- end inside cont -->



<link rel="stylesheet"  href="<?php echo Yii::app()->baseUrl; ?>/asset/js/light-gallery/css/lightGallery.css"/>
<script src="<?php echo Yii::app()->baseUrl; ?>/asset/js/light-gallery/js/lightGallery.js"></script>
<script>
  $(document).ready(function() {
    $(".light-gallery").lightGallery({
      thumbnail: false,
    });
  });
</script>