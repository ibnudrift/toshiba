<!-- start single split -->
<?php if ($category_id == 10): ?>
  <div class="lefts_c">
  <h6>REFINE BY</h6>
  <div class="filtering_data">
    <div class="list_filter">
      <p><b>TYPE</b></p>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          Single Split Inverter
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          Single Split Non Inverter
        </label>
      </div>

      <div class="clear"></div>
    </div>

    <div class="list_filter">
      <p><b>Capacity (PK)</b></p>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          1/2
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          3/4
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          1
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          1.5
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          2
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          2.5
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          3.5
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          4.5
        </label>
      </div>
      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>

  <div class="clear"></div>
</div>
<?php endif ?>
<!-- end single split -->

<!-- start multi split -->
<?php if ($category_id == 11): ?>
  <div class="lefts_c">
  <h6>REFINE BY</h6>
  <div class="filtering_data">
    <div class="list_filter">
      <p><b>TYPE</b></p>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          Multi Split Inverter
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          Multi Split Non Inverter
        </label>
      </div>

      <div class="clear"></div>
    </div>

    <div class="list_filter">
      <p><b>Capacity (PK)</b></p>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          1/2
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          3/4
        </label>
      </div>
      
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          1
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          1.5
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          2
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          2.5
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          3.5
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          4.5
        </label>
      </div>

      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>

  <div class="clear"></div>
</div>
<?php endif ?>
<!-- end multi split -->

<!-- start ducted -->
<?php if ($category_id == 12): ?>
  <div class="lefts_c">
  <h6>REFINE BY</h6>
  <div class="filtering_data">
    <div class="list_filter">
      <p><b>TYPE</b></p>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          Ducted Inverter
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          Ducted Non Inverter
        </label>
      </div>

      <div class="clear"></div>
    </div>

    <div class="list_filter">
      <p><b>Capacity (PK)</b></p>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          1.5
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          2
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          2.5
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          3
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          3.3
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          4
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          5
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          6
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          8
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          10
        </label>
      </div>

      <div class="clear"></div>
    </div>

    <div class="list_filter">
      <p><b>POWER SUPPLY</b></p>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          1-Phase
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          3-Phase
        </label>
      </div>

      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>

  <div class="clear"></div>
</div>
<?php endif ?>
<!-- end ducted -->


<!-- start cassette -->
<?php if ($category_id == 13): ?>
  <div class="lefts_c">
  <h6>REFINE BY</h6>
  <div class="filtering_data">
    <div class="list_filter">
      <p><b>TYPE</b></p>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          Cassette Inverter
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          Cassette Non Inverter
        </label>
      </div>

      <div class="clear"></div>
    </div>

    <div class="list_filter">
      <p><b>Capacity (PK)</b></p>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          1.5
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          2
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          2.5
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          3
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          3.3
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          4
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          5
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          6
        </label>
      </div>
      
      <div class="clear"></div>
    </div>

    <div class="list_filter">
      <p><b>Power Supply</b></p>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          1-Phase
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          3-Phase
        </label>
      </div>

      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>

  <div class="clear"></div>
</div>
<?php endif; ?>
<!-- end cassette -->


<!-- start floor standing -->
<?php if ($category_id == 14): ?>
  <div class="lefts_c">
  <h6>REFINE BY</h6>
  <div class="filtering_data">
    <div class="list_filter">
      <p><b>Capacity (PK)</b></p>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          2.5
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          5
        </label>
      </div>
      <div class="clear"></div>
    </div>
    <div class="list_filter">
      <p><b>Power Supply</b></p>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          1-Phase
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          3-Phase
        </label>
      </div>
      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>

  <div class="clear"></div>
</div>
<?php endif; ?>
<!-- end floor standing -->


<!-- start Ceiling -->
<?php if ($category_id == 15): ?>
  <div class="lefts_c">
  <h6>REFINE BY</h6>
  <div class="filtering_data">
    <div class="list_filter">
      <p><b>TYPE</b></p>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          Ceiling Inverter
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          Ceiling Non Inverter
        </label>
      </div>

      <div class="clear"></div>
    </div>

    <div class="list_filter">
      <p><b>Capacity (PK)</b></p>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          1.5
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          2
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          2.5
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          3
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          3.3
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          4
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          5
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          6
        </label>
      </div>

      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>

  <div class="clear"></div>
</div>
<?php endif ?>
<!-- end Ceiling -->


<!-- start VRF - side blow -->
<?php if ($category_id == 16): ?>
  <div class="lefts_c">
  <h6>REFINE BY</h6>
  <div class="filtering_data">
    <div class="list_filter">
      <?php 
      $var_indoor = array('Compact Cassette', '1-way Cassette', '2-way Cassette', '4-way Cassette', 'Slim Ducted', 'Concealed Duct High Static', 'Concealed Duct', 'Ceiling', 'High Wall', 'Floor Standing Cabinet', 'Floor Standing Concealed', 'Floor Standing', 'Floor Standing Ducted', 'Floor Standing Direct', 'Fresh Air Intake', 'Air-to-Air Heat Exchanger with DX', 'Air-to-Air Heat Exchanger');
      ?>
      <p><b>Indoor Unit</b></p>
      <?php foreach ($var_indoor as $key => $value): ?>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          <?php echo $value ?>
        </label>
      </div>
      <?php endforeach ?>

      <div class="clear"></div>
    </div>

    <div class="list_filter">
      <p><b>Capacity (PK)</b></p>
      <?php $var_capac_sideBlow = array('3/4','1','1.5','1.7','2','2.5','3','3.3','4','5','6','8','10','12','14','16','18','20','22','24','26','28','30','32','34','36','38','40','42','44','46','48','50','52','54','56','58','60'); ?>
      <?php foreach ($var_capac_sideBlow as $key => $value): ?>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          <?php echo $value ?>
        </label>
      </div>
      <?php endforeach ?>

      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>

  <div class="clear"></div>
</div>
<?php endif ?>
<!-- end VRF - side blow -->


<!-- start VRF - top blow -->
<?php if ($category_id == 17): ?>
  <div class="lefts_c">
  <h6>REFINE BY</h6>
  <div class="filtering_data">
    <div class="list_filter">
      <?php 
      $var_indoor = array('Compact Cassette', '1-way Cassette', '2-way Cassette', '4-way Cassette', 'Slim Ducted', 'Concealed Duct High Static', 'Concealed Duct', 'Ceiling', 'High Wall', 'Floor Standing Cabinet', 'Floor Standing Concealed', 'Floor Standing', 'Floor Standing Ducted', 'Floor Standing Direct', 'Fresh Air Intake', 'Air-to-Air Heat Exchanger with DX', 'Air-to-Air Heat Exchanger');
      ?>
      <p><b>Indoor Unit</b></p>
      <?php foreach ($var_indoor as $key => $value): ?>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          <?php echo $value ?>
        </label>
      </div>
      <?php endforeach ?>

      <div class="clear"></div>
    </div>

    <div class="list_filter">
      <p><b>Capacity (PK)</b></p>
      <?php $var_capac_sideBlow = array('3/4','1','1.5','1.7','2','2.5','3','3.3','4','5','6','8','10','12'); ?>
      <?php foreach ($var_capac_sideBlow as $key => $value): ?>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          <?php echo $value ?>
        </label>
      </div>
      <?php endforeach ?>

      <div class="clear"></div>
    </div>

    <div class="list_filter">
      <p><b>Power Supply</b></p>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          1-Phase
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" value="">
          3-Phase
        </label>
      </div>

      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>

  <div class="clear"></div>
</div>
<?php endif ?>
<!-- end VRF - top blow -->