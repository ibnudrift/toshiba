<section class="default_sc" id="block_homesection1">
	<div class="prelatife container">
		<div class="insides">
			<div class="tops_banner_home1">
				<div class="row">
					<div class="col-md-8">
						<div class="lefts_itm">
							<div class="block_human1">
								<img src="<?php echo $this->assetBaseurl ?>picts_human1_left_banner1.png" alt="">
							</div>
							<div class="block_frm_in">
								<p>What products are you looking for?</p>
				                <form action="<?php echo CHtml::normalizeUrl(array('/product/index')); ?>" method="get" class="form-inline">
				                  <div class="form-group">
				                  	<i class="fa fa-search"></i>
				                    <input type="text" name="q" value="<?php echo $_GET['q'] ?>" class="form-control" placeholder="SEARCH" required>
				                  </div>
				                  <button type="submit" class="btn btn-link"></button>
				                </form>
								<div class="clear"></div>
							</div>
							<div class="clear"></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="rights_itm">
							<img src="<?php echo $this->assetBaseurl ?>p_banner_home_right1.jpg" alt="" class="img img-fluid">
							<div class="info">
								<div class="row">
									<div class="col-xs-8">
										<p>Toshiba&rsquo;s latest innovation on<br>power saving revealed.</p>
									</div>
									<div class="col-xs-4 text-right">
										<a href="<?php echo CHtml::normalizeUrl(array('/home/video')); ?>" class="btn btn-link btns_playvideo">PLAY VIDEO</a>
									</div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear height-15"></div><div class="height-3"></div>
			<div class="lists_banner_home_d1">
				<div class="row">
					<div class="col-md-4">
						<div class="items prelatife">
							<a href="<?php echo $this->setting['home_banner_url_1'] ?>">
							<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(375,182, '/images/static/'.$this->setting['home_banner_image_1'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid">
							</a>
						</div>
					</div>
					<div class="col-md-4">
						<div class="items prelatife">
							<a href="<?php echo $this->setting['home_banner_url_2'] ?>">
							<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(375,182, '/images/static/'.$this->setting['home_banner_image_2'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid">
							</a>
						</div>
					</div>
					<div class="col-md-4">
						<div class="items prelatife">
							<a href="<?php echo $this->setting['home_banner_url_3'] ?>">
							<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(375,182, '/images/static/'.$this->setting['home_banner_image_3'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid">
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>