<footer class="foot">
	<div class="blocks_top_footer_green">
		<div class="prelatife container">
			<div class="row">
				<div class="col-md-7 col-sm-7">
					<div class="d-inline prelatife bloc_leaf"><img src="<?php echo $this->assetBaseurl ?>backs_bottom_left_footer_green.png" alt="" class="img img-fluid"></div>
					<div class="d-inline tx_white"><?php echo Tt::t('front', 'Advancing the <b>eco</b> -evolution') ?></div>
				</div>
				<div class="col-md-5 col-sm-5 text-right">
					<div class="bloc_green_globe"><img src="<?php echo $this->assetBaseurl ?>backs_bottom_right_footer_green.png" alt="" class="img img-fluid"></div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="prelatife container"> 
		<div class="insides_footer">
			<div class="clear height-30"></div> 
			<div class="tops_footer">
				<div class="row">
					<div class="col-md-8">
						<div class="blk_menuFooter">
							<div class="row default">
								<div class="col-md-4 col-sm-4">
									<h6>Products</h6>
									<?php
									$criteria = new CDbCriteria;
									$criteria->with = array('description');
									$criteria->addCondition('t.parent_id = :parent_id');
									$criteria->params[':parent_id'] = 0;
									$criteria->addCondition('t.type = :type');
									$criteria->params[':type'] = 'category';
									$criteria->addCondition('description.language_id = :language_id');
									$criteria->params[':language_id'] = $this->languageID;
									$criteria->limit = 3;
									$criteria->order = 'sort ASC';
									$subCategory = PrdCategory::model()->findAll($criteria);

									?>
									<ul class="list-unstyled">
									<?php foreach ($subCategory as $key => $value): ?>
										<li><a  href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'category'=>$value->id, 'lang'=>Yii::app()->language)); ?>"><?php echo $value->description->name ?></a></li>
									<?php endforeach; ?>
									</ul>
								</div>
								<div class="col-md-4 col-sm-4">
									<h6>Service</h6>
									<ul class="list-unstyled">
										<li><a href="<?php echo CHtml::normalizeUrl(array('/home/services/', 'loc'=>1, 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'Service and maintenace') ?></a></li>
					                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/services/', 'loc'=>2, 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'Part center') ?></a></li>
					                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/services/', 'loc'=>3, 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'Warranty') ?></a></li>
									</ul>
								</div>
								<div class="col-md-4 col-sm-4">
									<h6>Information</h6> 
									<ul class="list-unstyled">
										<li><a href="<?php echo CHtml::normalizeUrl(array('/home/dealer', 'loc'=>'dealer-location', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'Dealer Locator') ?></a></li>
										<li><a href="<?php echo CHtml::normalizeUrl(array('/home/video', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'Videos') ?></a></li>
										<li><a href="<?php echo CHtml::normalizeUrl(array('/home/event', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'Events') ?></a></li>
										<li><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'About') ?></a></li>
										<li><a href="<?php echo CHtml::normalizeUrl(array('/home/contactus', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'Contact') ?></a></li>
									</ul>
									
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="rights_info">
							<h6>PT. Berca Carrier Indonesia</h6>
							<p>Telephone. (021) 2664 5888<br />
							Email. <a href="mailto:sales.toshiba@carrier.co.id">sales.toshiba@carrier.co.id</a><br />
							Gedung Pusat Niaga 4th Floor<br />
							Arena PRJ Kemayoran<br />
							Jakarta 10620, Indonesia</p>
							<div class="clear height-25"></div>
							<div class="socmed_footer">
								<p><?php echo Tt::t('front', 'Connect With Us') ?></p> <div class="clear"></div>
								<?php if ($this->setting['url_twitter'] != ''): ?>
								<a target="_blank" href="<?php echo $this->setting['url_twitter'] ?>"><i class="fa fa-twitter"></i></a>&nbsp;&nbsp;&nbsp;
								<?php endif ?>
								<?php if ($this->setting['url_instagram'] != ''): ?>
								<a target="_blank" href="<?php echo $this->setting['url_instagram'] ?>"><i class="fa fa-instagram"></i></a>&nbsp;&nbsp;&nbsp;
								<?php endif ?>
								<?php if ($this->setting['url_facebook'] != ''): ?>
								<a target="_blank" href="<?php echo $this->setting['url_facebook'] ?>"><i class="fa fa-facebook-square"></i></a>&nbsp;&nbsp;&nbsp;
								<?php endif ?>
								<?php if ($this->setting['url_linkedin'] != ''): ?>
								<a target="_blank" href="<?php echo $this->setting['url_linkedin'] ?>"><i class="fa fa-linkedin"></i></a>&nbsp;&nbsp;&nbsp;
								<?php endif ?>
								<?php if ($this->setting['url_youtube'] != ''): ?>
								<a target="_blank" href="<?php echo $this->setting['url_youtube'] ?>"><i class="fa fa-youtube"></i></a>
								<?php endif ?>
								<div class="clear"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clear height-25"></div>
		</div>
	</div>
	<div class="btms_mnfooter">
		<div class="prelatife container">
			<div class="clear height-20"></div>
			<div class="row">
				<div class="col-md-12 col-sm-12"> 
					<div class="lgo_footers d-inline">
						<a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>"><img src="<?php echo $this->assetBaseurl ?>lgo_footer_toshiba.png" alt="" class="img img-fluid"></a>
					</div>
					<p class="t-copyrights d-inline padding-left-15 padding-top-30">
						Copyright &copy; 2017 PT Berca Carrier Indonesia<br>Trademarks are proprietary to PT Berca Carrier Indonesia 
					</p>
				</div>
				<div class="col-md-3 text-right col-sm-3 hide hidden">
					<div class="clear height-40"></div>
					<p class="t-copyrights ">
						Website Design by <a href="http://www.markdesign.net/" target="_blank" title="Website Design Surabaya">Mark Design.</a>
					</p>
					
					<div class="clear"></div>
				</div>
			</div>
			<div class="clear height-15"></div>
		</div>
		<div class="clear"></div>
	</div>
</footer>