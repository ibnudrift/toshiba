<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<?php echo $this->renderPartial('//layouts/_header', array()); ?>

<!-- Start fcs -->
<div class="fcs-wrapper outers_fcs_wrapper prelatife">
    <div id="myCarousel_home" class="carousel fade" data-ride="carousel" data-interval="6000">
        <?php
        $criteria=new CDbCriteria;
        $criteria->with = array('description');
        $criteria->addCondition('description.language_id = :language_id');
        $criteria->params[':language_id'] = $this->languageID;
        $criteria->addCondition('topik_id = :type');
        $criteria->params[':type'] = 0;
        $criteria->group = 't.id';
        $criteria->order = 't.id ASC';
        $slide = Slide::model()->with(array('description'))->findAll($criteria);
        ?>
        <ol class="carousel-indicators">
            <?php foreach ($slide as $key => $value): ?>
                <li data-target="#myCarousel_home" data-slide-to="<?php echo $key; ?>" <?php if($key == 0){ ?>class="active"><?php  } ?></li>
            <?php endforeach; ?>
        </ol>
        <div class="carousel-inner">
            <?php foreach($slide as $key => $value): ?>
            <div class="item <?php if ($key == 0): ?>active<?php endif ?>">
                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1900,850, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid full_img">
                <div class="carousel-caption">
                    <div class="container prelatife">
                        <div class="wrap_info_slide">
                            <div class="ins">
                                <h3><?php echo $value->description->title ?></h3>
                                <h6><?php echo nl2br($value->description->subtitle) ?></h6>
                                <?php echo $value->description->content ?>
                                <a href="<?php echo $value->description->url ?>" class="btn btn-link btns_vc_slide"><?php echo $value->description->url_teks ?></a>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach ?>
        </div>
    </div>

    <div class="clear"></div>
</div>
<!-- End fcs -->

<?php echo $content ?>

<?php echo $this->renderPartial('//layouts/_footer', array()); ?>


<script type="text/javascript">
$(function(){
    var $wWidths = $(window).width();
    if ($wWidths > 768){
        // set full screen Fcs
        var $item = $('.carousel .item'); 
        var $wHeight = $(window).height();
        $item.eq(0).addClass('active');
        $item.height($wHeight); 
        $item.addClass('full-screen');

        $('.carousel img').each(function() {
          var $src = $(this).attr('src');
          var $color = $(this).attr('data-color');
          $(this).parent().css({
            'background-image' : 'url(' + $src + ')',
            'background-color' : $color
          });
          $(this).remove();
        });

        $(window).on('resize', function (){
          $wHeight = $(window).height();
          $item.height($wHeight);
        });

        $('.carousel').carousel({
          interval: 5000,
          pause: "false"
        });
    }
});
</script>
<script type="text/javascript">
$(function(){
    if ($(window).width() > 1200){
        $('.outers_back-header').addClass('posabs_fulls');
    } else {
        $('.outers_back-header').removeClass('posabs_fulls');
    }
});
</script>
<?php $this->endContent(); ?>