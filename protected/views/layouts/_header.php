<?php 
    $e_activemenu = $this->action->id;
    $controllers_ac = $this->id;
    $session=new CHttpSession;
    $session->open();
    $login_member = $session['login_member'];

    $active_menu_pg = $controllers_ac.'/'.$e_activemenu;

    $criteria = new CDbCriteria;
    $criteria->with = array('description');
    $criteria->addCondition('parent_id = 0');
    $criteria->addCondition('type = "category"');
    $criteria->addCondition('description.language_id = :language_id');
    $criteria->params[':language_id'] = $this->languageID;
    $criteria->order = 'sort ASC';
    $dataCategory = PrdCategory::model()->findAll($criteria);
?>
<div class="outers_back-header">
<header class="head">
  <div class="d-none d-sm-block prelatife">
    <div class="afx_rights_headertop_fn">
      <!-- <span>(021) 26645888</span> -->
      <span><?php echo $this->setting['contact_float_phone'] ?></span>
      <small class="inf">Show/Hide</small>
    </div>
    <div class="prelatife container">
      <div class="ins">
      <div class="tops">
        <div class="row">
          <div class="col-md-7">
            <div class="left_tp_header">
              <a href="<?php echo CHtml::normalizeUrl(array('/home/login', 'sub'=>'warranty', 'lang'=>Yii::app()->language)); ?>" class="btn btn-link backs_blue1 d-inline"><?php echo Tt::t('front', 'WARRANTY REGISTRATION') ?></a>
              <a href="<?php echo CHtml::normalizeUrl(array('/home/login', 'sub'=>'dealer', 'lang'=>Yii::app()->language)); ?>" class="btn btn-link backs_blue2 d-inline"><i class="fa fa-lock"></i> &nbsp;<?php echo Tt::t('front', 'DEALER LOGIN') ?></a>
              <a href="<?php echo CHtml::normalizeUrl(array('/home/login', 'sub'=>'installer', 'lang'=>Yii::app()->language)); ?>" class="btn btn-link backs_blue3 d-inline"><i class="fa fa-lock"></i> &nbsp;<?php echo Tt::t('front', 'INSTALLER LOGIN') ?></a>
            </div>
          </div>
          <div class="col-md-5 text-right">
            <div class="rights_tp_headers">

              <div class="box_search_hdr">
                <form action="<?php echo CHtml::normalizeUrl(array('/product/index', 'lang'=>Yii::app()->language)); ?>" method="get" class="form-inline">
                  <div class="form-group">
                    <input type="text" name="q" value="<?php echo $_GET['q'] ?>" class="form-control" placeholder="Search.." required>
                  </div>
                  <button class="btn btn-link"><i class="fa fa-search"></i></button>
                </form>
                <div class="clear"></div>
              </div>
              <div class="bxn_right_language">
                  <?php
                  $get = $_GET;
                  $get['lang'] = 'en';
                  ?>
                    <a <?php if (Yii::app()->language == 'en'): ?>class="active"<?php endif ?> href="<?php echo $this->createUrl($this->route, $get) ?>">EN</a>
                    <?php /*&nbsp;&nbsp;|&nbsp;&nbsp;
                      <?php
                      $get['lang'] = 'id';
                      ?>
                    <a <?php if (Yii::app()->language == 'id'): ?>class="active"<?php endif ?> href="<?php echo $this->createUrl($this->route, $get) ?>">IN</a>*/ ?>
              </div>
              <div class="clear"></div>
            </div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
      <div class="bottoms">
        <div class="row">
          <div class="col-md-5">
            <div class="lgo_web_header d-inline-block align-middle padding-right-30"><a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>"><img src="<?php echo $this->assetBaseurl ?>lgo_header_web.jpg" alt="" class="img img-fluid"></a></div>
            <div class="d-inline-block align-middle taglines_hdr1">
              <img src="<?php echo $this->assetBaseurl ?>lgs_tagline_header1.png" alt="" class="img img-fluid">
            </div>
          </div>
          <div class="col-md-7">
            <div class="clear height-45"></div><div class="height-2"></div>
            <div class="top-menu">
              <ul class="list-inline">
                <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'HOME') ?></a></li>
                <li class="list-inline-item <?php echo ($controllers_ac == 'product')? 'active':''; ?> dropdown first"><a class="dropdown-toggle" href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'products') ?></a>
                   <ul class="dropdown-menu">
                    <?php
                      $criteria = new CDbCriteria;
                      $criteria->with = array('description');
                      $criteria->addCondition('t.parent_id = :parent_id');
                      $criteria->params[':parent_id'] = 0;
                      $criteria->addCondition('t.type = :type');
                      $criteria->params[':type'] = 'category';
                      $criteria->addCondition('description.language_id = :language_id');
                      $criteria->params[':language_id'] = $this->languageID;
                      // $criteria->limit = 3;
                      $criteria->order = 'sort ASC';
                      $subCategory = PrdCategory::model()->findAll($criteria);
                    ?>
                    <?php foreach ($subCategory as $key => $value): ?>
                      <?php
                      $criteria = new CDbCriteria;
                      $criteria->with = array('description');
                      $criteria->addCondition('t.parent_id = :parent_id');
                      $criteria->params[':parent_id'] = $value->id;
                      $criteria->addCondition('t.type = :type');
                      $criteria->params[':type'] = 'category';
                      $criteria->addCondition('description.language_id = :language_id');
                      $criteria->params[':language_id'] = $this->languageID;
                      // $criteria->limit = 3;
                      $criteria->order = 'sort ASC';
                      $subCategory2 = PrdCategory::model()->findAll($criteria);
                      ?>
                    <li class="dropdown">
                      <a class="dropdown-toggle" href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'category'=>$value->id, 'lang'=>Yii::app()->language)); ?>"><?php echo $value->description->name ?></a>
                      <ul class="dropdown-menu">
                        <?php foreach ($subCategory2 as $k => $v): ?>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$v->id, 'lang'=>Yii::app()->language)); ?>"><?php echo $v->description->name ?></a></li>
                        <?php endforeach ?>
                      </ul>
                    </li>
                    <?php endforeach ?>
                  </ul>
                </li>
                <li class="list-inline-item <?php echo ($active_menu_pg == 'home/services')? 'active':''; ?> dropdown first">
                  <a class="dropdown-toggle" href="<?php echo CHtml::normalizeUrl(array('/home/services', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'SERVICES') ?></a>
                  <ul class="dropdown-menu onesub">
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/services/', 'loc'=>1, 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'Service and maintenace') ?></a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/services/', 'loc'=>2, 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'Part center') ?></a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/services/', 'loc'=>3, 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'Warranty') ?></a></li>
                  </ul>
                </li>

                <li class="list-inline-item <?php echo ($active_menu_pg == 'home/dealer')? 'active':''; ?> dropdown first">
                  <a class="dropdown-toggle" role="button" href="javascript:return false;"><?php echo Tt::t('front', 'dealer locator') ?></a>
                  <ul class="dropdown-menu onesub">
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/dealer', 'loc'=>'dealer-location', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'Dealer Location') ?></a></li>
                    <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/home/dealer', 'loc'=>'asp-location', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'ASP Location') ?></a></li> -->
                  </ul>
                </li>

                <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/video', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'VIDEOS') ?></a></li>

                <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/blog/index', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'BLOG') ?></a></li>

                <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'about') ?></a></li>
                <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/contactus', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'contact') ?></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
      <!-- end bottoms header -->
      </div>

      <div class="clear"></div>
    </div>

  </div>
  <!-- end bottom -->
  
  <div class="d-block d-sm-none">
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>">
              <img src="<?php echo $this->assetBaseurl ?>lgo_header_web.jpg" alt="" class="img img-fluid">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>


          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'HOME') ?></a></li>
                <li class="<?php echo ($controllers_ac == 'product')? 'active':''; ?> dropdown first"><a class="dropdown-toggle" href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'products') ?></a>
                   <ul class="dropdown-menu">
        <?php
            $criteria = new CDbCriteria;
            $criteria->with = array('description');
            $criteria->addCondition('t.parent_id = :parent_id');
            $criteria->params[':parent_id'] = 0;
            $criteria->addCondition('t.type = :type');
            $criteria->params[':type'] = 'category';
            $criteria->addCondition('description.language_id = :language_id');
            $criteria->params[':language_id'] = $this->languageID;
            // $criteria->limit = 3;
            $criteria->order = 'sort ASC';
            $subCategory = PrdCategory::model()->findAll($criteria);
            ?>
                    <?php foreach ($subCategory as $key => $value): ?>
<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.parent_id = :parent_id');
$criteria->params[':parent_id'] = $value->id;
$criteria->addCondition('t.type = :type');
$criteria->params[':type'] = 'category';
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $criteria->limit = 3;
$criteria->order = 'sort ASC';
$subCategory2 = PrdCategory::model()->findAll($criteria);

?>
                    <li class="dropdown">
                      <a class="dropdown-toggle" href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'category'=>$value->id, 'lang'=>Yii::app()->language)); ?>"><?php echo $value->description->name ?></a>
                      <ul class="dropdown-menu">
                        <?php foreach ($subCategory2 as $k => $v): ?>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$v->id, 'lang'=>Yii::app()->language)); ?>"><?php echo $v->description->name ?></a></li>
                        <?php endforeach ?>
                      </ul>
                    </li>
                    <?php endforeach ?> 
                  </ul>
                </li>
                <li class="<?php echo ($active_menu_pg == 'home/services')? 'active':''; ?> dropdown first">
                  <a class="dropdown-toggle" href="<?php echo CHtml::normalizeUrl(array('/home/services', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'SERVICES') ?></a>
                  <ul class="dropdown-menu onesub">
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/services/', 'loc'=>1, 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'Service and maintenace') ?></a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/services/', 'loc'=>2, 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'Part center') ?></a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/services/', 'loc'=>3, 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'Warranty') ?></a></li>
                  </ul>
                </li>

                <li class="<?php echo ($active_menu_pg == 'home/dealer')? 'active':''; ?> dropdown first">
                  <a class="dropdown-toggle" role="button" href="javascript:return false;"><?php echo Tt::t('front', 'dealer locator') ?></a>
                  <ul class="dropdown-menu onesub">
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/dealer', 'loc'=>'dealer-location', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'Dealer Location') ?></a></li>
                    <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/home/dealer', 'loc'=>'asp-location', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'ASP Location') ?></a></li> -->
                  </ul>
                </li>

                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/video', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'VIDEOS') ?></a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/blog/index', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'BLOG') ?></a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'about') ?></a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/contactus', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'contact') ?></a></li>
            </ul>
            <div class="clear height-10"></div>
            <div class="bloc_warranty_reg">
              <a href="<?php echo CHtml::normalizeUrl(array('/home/login', 'sub'=>'warranty')); ?>" class="btn btn-info d-block mb-1 first"><?php echo Tt::t('front', 'WARRANTY REGISTRATION') ?></a>
              <a href="<?php echo CHtml::normalizeUrl(array('/home/login', 'sub'=>'dealer')); ?>" class="btn btn-info d-block mb-1 second"><i class="fa fa-lock"></i> &nbsp;<?php echo Tt::t('front', 'DEALER LOGIN') ?></a>
              <a href="<?php echo CHtml::normalizeUrl(array('/home/login', 'sub'=>'installer')); ?>" class="btn btn-info d-block mb-1 third"><i class="fa fa-lock"></i> &nbsp;<?php echo Tt::t('front', 'INSTALLER LOGIN') ?></a>
            </div>
            <div class="clear"></div>
            <div class="d-inline padding-left-0 bxn_right_language">
<?php
$get = $_GET;
$get['lang'] = 'en';
?>
                    <a <?php if (Yii::app()->language == 'en'): ?>class="active"<?php endif ?> href="<?php echo $this->createUrl($this->route, $get) ?>">EN</a>
                    <?php /*&nbsp;&nbsp;|&nbsp;&nbsp; 
<?php
$get['lang'] = 'id';
?>
                    <a <?php if (Yii::app()->language == 'id'): ?>class="active"<?php endif ?> href="<?php echo $this->createUrl($this->route, $get) ?>">IN</a>*/ ?>
            </div>
            <div class="clear"></div>
          </div>

      </nav>

      <div class="clear"></div>
    </div>
    <!-- end bottom responsive -->

  <div class="clear"></div>
</header>
</div>

<?php /*
<script type="text/javascript">
  $(document).ready(function(){
      $('.nl_popup a').live('hover', function(){
          $('.popup_carts_header').fadeIn();
      });
      $('.popup_carts_header').live('mouseleave', function(){
        setTimeout(function(){ 
            $('.popup_carts_header').fadeOut();
        }, 500);
      });
  });
</script>

<script type="text/javascript">
    $(function(){
      $('#myAffix').affix({
        offset: {
          top: 300
        }
      })
    })
  </script>

<section id="myAffix" class="header-affixs affix-top">
  <div class="clear height-5"></div>
  <div class="prelatife container">
    <div class="row">
      <div class="col-md-3 col-sm-3">
        <div class="lgo-web-web_affix">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
            <img src="<?php echo $this->assetBaseurl ?>lgo_stumble_wt_txt.png" alt="" class="img img-fluid d-inline" style="max-width: 75px;">
            STUMBLE UPON
          </a>
        </div>
      </div>
      <div class="col-md-9 col-sm-9">
        <div class="text-right"> 
          <div class="clear height-20"></div>
          <div class="menu-taffix">
            <ul class="list-inline">
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">HOME</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/product/index')); ?>">BUY COFFEE</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/howto')); ?>">HOW TO ORDER</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/wholesale')); ?>">WHOLESALE</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">ABOUT US</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/event')); ?>">EVENTS</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">CONTACT US</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
</section>
*/ ?>